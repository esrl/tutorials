library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity xadc_v1 is
    Port ( 
    clk_125M : in STD_LOGIC;
    adc_vn14 : in STD_LOGIC;
    adc_vp14 : in STD_LOGIC;
    adc_vn7 : in STD_LOGIC;
    adc_vp7 : in STD_LOGIC;
    adc_vn15 : in STD_LOGIC;
    adc_vp15 : in STD_LOGIC;
    adc_vn6 : in STD_LOGIC;
    adc_vp6 : in STD_LOGIC;
    adc_out  : out STD_LOGIC_VECTOR(31 downto 0);
    data_req : out STD_LOGIC := '0';
    data_ack : in STD_LOGIC := '0'
);
end xadc_v1;

architecture Behavioral of xadc_v1 is
--signals for ADC output
signal read_address : STD_LOGIC_VECTOR(6 downto 0) := "0000000";    --initialize as 0
signal data_out     : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
signal ready_flag   : STD_LOGIC := '0';
signal adc_internal : STD_LOGIC_VECTOR(31 downto 0) := x"00000000";
signal adc_enable   : STD_LOGIC := '0';

--signals for prescaling main clock
signal prescale_clock : unsigned(31 downto 0) := x"00000000";
signal adc_clock      : std_logic;

type adc_state_type is (adc_wait, adc_update_address, adc_read_data);
signal adc_state, adc_next_state  : adc_state_type;

component adc_hw_wrapper is
    port (
        Vaux14_0_v_n : in STD_LOGIC;
        Vaux14_0_v_p : in STD_LOGIC;
        Vaux15_0_v_n : in STD_LOGIC;
        Vaux15_0_v_p : in STD_LOGIC;
        Vaux6_0_v_n : in STD_LOGIC;
        Vaux6_0_v_p : in STD_LOGIC;
        Vaux7_0_v_n : in STD_LOGIC;
        Vaux7_0_v_p : in STD_LOGIC;
        daddr_in_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        dclk_in_0 : in STD_LOGIC;
        den_in_0 : in STD_LOGIC;
        do_out_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        drdy_out_0 : out STD_LOGIC
      );
end component;

begin

adc_hw_wrapper_instance: adc_hw_wrapper
    port map(
        Vaux14_0_v_n => adc_vn14,
        Vaux14_0_v_p => adc_vp14,
        Vaux15_0_v_n => adc_vn15,
        Vaux15_0_v_p => adc_vp15,
        Vaux6_0_v_n => adc_vn6,
        Vaux6_0_v_p => adc_vp6,
        Vaux7_0_v_n => adc_vn7,
        Vaux7_0_v_p => adc_vp7,
        daddr_in_0 => read_address,
        dclk_in_0 => clk_125M,
        den_in_0 => '1',                        --use adc_enable when simulating
        do_out_0 => data_out,
        drdy_out_0 => ready_flag
    );

adc_clock <= (prescale_clock(1));               

--data_ready: process(ready_flag)
--begin
--   if rising_edge(ready_flag) then
--           adc_enable <= '1';                  --use when simulating, enable ADC
--   end if;    
--end process; 
     
prescale: process(clk_125M)
begin
   if rising_edge(clk_125M) then
       prescale_clock <= prescale_clock + 1;
   end if;    
end process; 

NEXT_STATE_PROC: process (clk_125M)    
begin
    if rising_edge(clk_125M) then
        adc_state <= adc_next_state; 
    end if;
end process;

sensor_proc: process(adc_clock)

variable counter : integer range 0 to 4 := 4;
variable data_ack_flag_int   : std_logic := '0';

begin
if rising_edge(adc_clock) then
    
    case(adc_state) is
    when adc_wait =>    
            data_req <= '1';
            data_ack_flag_int := data_ack;
            if data_ack_flag_int = '1' then
               data_req <= '0';
               adc_out <= adc_internal;
               if counter < 3 then counter := counter + 1; else counter := 0; end if;
               adc_next_state <= adc_update_address; 
            end if;     
    when adc_update_address => 
        case counter is
            when 0 => read_address <= "0011110"; 
            when 1 => read_address <= "0010111";
            when 2 => read_address <= "0011111";
            when 3 => read_address <= "0010110";
            when others => read_address <= "0000000";
        end case;
        adc_next_state <= adc_read_data;
    when adc_read_data =>
            case read_address is
                when "0011110" => adc_internal <= x"01" & x"000" & data_out(15 downto 4);    --VAUX14
                when "0010111" => adc_internal <= x"02" & x"000" & data_out(15 downto 4);    --VAUX7
                when "0011111" => adc_internal <= x"03" & x"000" & data_out(15 downto 4);    --VAUX15
                when "0010110" => adc_internal <= x"04" & x"000" & data_out(15 downto 4);    --VAUX6
                when others => adc_internal <= x"00000000"; 
            end case;     
        adc_next_state <= adc_wait;
    end case;

end if;
end process;


end Behavioral;