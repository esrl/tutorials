----------------------------------------------------------------------------------
-- Company: University of Southern Denmark
-- Engineer: Beck Strohmer
-- 
-- Create Date: 02/21/2018 10:19:21 AM
-- Design Name: Memory Interface
-- Module Name: top - Behavioral
-- Project Name: Flexible High-Performance Hardware Framework for Modular Experimental Robotics
-- Target Devices: ZYBO-Z7 
-- Tool Versions: Vivado 2017.4
-- Description: Single node function (main memory interface)
--              Component descriptions:
--              system_wrapper: local BRAM for communication with the bare-metal core
--              main_memory: interface for communication between memory layer and software layer
--              pc_pwm: PWM generator, I/O layer component
--              xadc: analog input translation, I/O layer component
-- 
-- Dependencies: 
-- 
-- Revision: 2
-- Revision 0.01 - Single node only version
--                 PWM component for control of servo motors
--                 Input sensors through XADC PMOD
-- License:     Copyright (C) 2019  Beck Strohmer

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is
    Port ( 
        clock_125M      : in STD_LOGIC;
        Vaux_v_n14      : in STD_LOGIC;
        Vaux_v_p14      : in STD_LOGIC;
        Vaux_v_n7       : in STD_LOGIC;
        Vaux_v_p7       : in STD_LOGIC;
        Vaux_v_n15      : in STD_LOGIC;
        Vaux_v_p15      : in STD_LOGIC;
        Vaux_v_n6       : in STD_LOGIC;
        Vaux_v_p6       : in STD_LOGIC;
        sw_setting      : in STD_LOGIC_VECTOR ( 3 downto 0 );       --Switches 
        motor_out       : out STD_LOGIC_VECTOR ( 15 downto 0 );     --PWM signal to servos (JC & JD)
        led_indicator   : out STD_LOGIC_VECTOR ( 3 downto 0 );
        system_test_bit : out STD_LOGIC                            --system speed test bit                       

    );
end top;

architecture Behavioral of top is

signal BRAM_PORTB_0_addr_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_raddr_signal  : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_clk_signal    : STD_LOGIC;
signal BRAM_PORTB_0_din_signal    : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_dout_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_en_signal     : STD_LOGIC;
signal BRAM_PORTB_0_rst_signal    : STD_LOGIC;
signal BRAM_PORTB_0_we_signal     : STD_LOGIC_VECTOR ( 3 downto 0 );
signal BRAM_PORTB_1_addr_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_raddr_signal  : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_clk_signal    : STD_LOGIC;
signal BRAM_PORTB_1_din_signal    : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_dout_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_en_signal     : STD_LOGIC;
signal BRAM_PORTB_1_rst_signal    : STD_LOGIC;
signal BRAM_PORTB_1_we_signal     : STD_LOGIC_VECTOR ( 3 downto 0 );
signal interrupt_flag_signal      : STD_LOGIC := '0';

signal data_req1, data_req2, data_ack1, data_ack2 : STD_LOGIC := '0';
signal clock_50M_signal         : STD_LOGIC;

--Interface to main memory state machine
signal motor_data_signal        : STD_LOGIC_VECTOR(31 downto 0);
signal motor_data_flag_signal   : STD_LOGIC := '0';
signal sensor_data_signal       : STD_LOGIC_VECTOR ( 31 downto 0 );
signal test_bit_signal          : STD_LOGIC := '0';

--Signals for PWM
signal pwm1_signal,pwm2_signal,pwm3_signal,pwm4_signal,pwm5_signal,pwm6_signal,pwm7_signal,pwm8_signal,pwm9_signal,pwm10_signal,pwm11_signal,pwm12_signal,pwm13_signal,pwm14_signal,pwm15_signal,pwm16_signal : STD_LOGIC;

--attribute mark_debug : string;
--attribute mark_debug of motor_data_signal : signal is "true";
--attribute mark_debug of BRAM_PORTB_0_addr_signal : signal is "true";


component system_wrapper is
  port (
      BRAM_PORTB_0_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_0_clk : in STD_LOGIC;
      BRAM_PORTB_0_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_0_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_0_en : in STD_LOGIC;
      BRAM_PORTB_0_rst : in STD_LOGIC;
      BRAM_PORTB_0_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
      BRAM_PORTB_1_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_1_clk : in STD_LOGIC;
      BRAM_PORTB_1_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_1_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_1_en : in STD_LOGIC;
      BRAM_PORTB_1_rst : in STD_LOGIC;
      BRAM_PORTB_1_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
      DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
      DDR_cas_n : inout STD_LOGIC;
      DDR_ck_n : inout STD_LOGIC;
      DDR_ck_p : inout STD_LOGIC;
      DDR_cke : inout STD_LOGIC;
      DDR_cs_n : inout STD_LOGIC;
      DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
      DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_odt : inout STD_LOGIC;
      DDR_ras_n : inout STD_LOGIC;
      DDR_reset_n : inout STD_LOGIC;
      DDR_we_n : inout STD_LOGIC;
      FIXED_IO_ddr_vrn : inout STD_LOGIC;
      FIXED_IO_ddr_vrp : inout STD_LOGIC;
      FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
      FIXED_IO_ps_clk : inout STD_LOGIC;
      FIXED_IO_ps_porb : inout STD_LOGIC;
      FIXED_IO_ps_srstb : inout STD_LOGIC;
      IRQ : in STD_LOGIC
  );
end component;

component main_mem_v7 is
    Port ( 
        clock           : in STD_LOGIC;
        BRAM_PORTB_0_addr : out STD_LOGIC_VECTOR ( 31 downto 0 );               
        BRAM_PORTB_0_we   : out STD_LOGIC_VECTOR ( 3 downto 0 );
        BRAM_PORTB_0_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
        BRAM_PORTB_1_addr : out STD_LOGIC_VECTOR ( 31 downto 0 );        
        BRAM_PORTB_1_din  : out STD_LOGIC_VECTOR ( 31 downto 0 );       
        BRAM_PORTB_1_we   : out STD_LOGIC_VECTOR ( 3 downto 0 );
        sensor_data     : in STD_LOGIC_VECTOR ( 31 downto 0 );
        motor_data      : out STD_LOGIC_VECTOR ( 31 downto 0 );
        data_req_out    : out STD_LOGIC;
        data_req_in     : in STD_LOGIC;
        data_ack_in     : in STD_LOGIC;
        data_ack_out    : out STD_LOGIC;
        system_response_out : out STD_LOGIC;
        interrupt_flag  : out STD_LOGIC
     );
end component;

component pc_pwm_v11 is
    Port ( 
           clk_50MHz      : in STD_LOGIC;
           data_req_pwm   : in STD_LOGIC;
           data_ack_pwm   : out STD_LOGIC;
           motor_data_pwm : in STD_LOGIC_VECTOR (31 downto 0);
           pulse_out      : out STD_LOGIC_VECTOR (15 downto 0)
           );
end component;

component xadc_v1 is
    Port ( 
        clk_125M : in STD_LOGIC;
        adc_vn14 : in STD_LOGIC;
        adc_vp14 : in STD_LOGIC;
        adc_vn7  : in STD_LOGIC;
        adc_vp7  : in STD_LOGIC;
        adc_vn15 : in STD_LOGIC;
        adc_vp15 : in STD_LOGIC;
        adc_vn6  : in STD_LOGIC;
        adc_vp6  : in STD_LOGIC;
        adc_out  : out STD_LOGIC_VECTOR(31 downto 0);
        data_req : out STD_LOGIC;
        data_ack : in STD_LOGIC
    );
end component;

component clock_50M_wrapper is
  port (
    clk_out1_0 : out STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    sys_clock : in STD_LOGIC
  );
end component;

begin

motor_out(0) <= pwm1_signal;
motor_out(1) <= pwm2_signal;
motor_out(2) <= pwm3_signal;
motor_out(3) <= pwm4_signal;
motor_out(4) <= pwm5_signal;
motor_out(5) <= pwm6_signal;
motor_out(6) <= pwm7_signal;
motor_out(7) <= pwm8_signal;
motor_out(8) <= pwm9_signal;
motor_out(9) <= pwm10_signal;
motor_out(10) <= pwm11_signal;
motor_out(11) <= pwm12_signal;
motor_out(12) <= pwm13_signal;
motor_out(13) <= pwm14_signal;
motor_out(14) <= pwm15_signal;
motor_out(15) <= pwm16_signal;

system_test_bit <= test_bit_signal;


wrapper_instance: system_wrapper
    port map(
        BRAM_PORTB_0_addr => BRAM_PORTB_0_addr_signal,
        BRAM_PORTB_0_clk => clock_125M,
        BRAM_PORTB_0_din => BRAM_PORTB_1_din_signal,
        BRAM_PORTB_0_dout => BRAM_PORTB_0_dout_signal,
        BRAM_PORTB_0_en => '1',
        BRAM_PORTB_0_rst => '0',
        BRAM_PORTB_0_we => BRAM_PORTB_0_we_signal,
        BRAM_PORTB_1_addr => BRAM_PORTB_1_addr_signal,
        BRAM_PORTB_1_clk => clock_125M,
        BRAM_PORTB_1_din => BRAM_PORTB_1_din_signal,
        BRAM_PORTB_1_dout => BRAM_PORTB_1_dout_signal,
        BRAM_PORTB_1_en => '1',
        BRAM_PORTB_1_rst => '0',
        BRAM_PORTB_1_we => BRAM_PORTB_1_we_signal,
        IRQ => interrupt_flag_signal
    );

main_mem_instance: main_mem_v7   
    port map( 
            clock           => clock_125M,
            BRAM_PORTB_0_addr => BRAM_PORTB_0_addr_signal,   --(output) address in BRAM to read from (motor data)
            BRAM_PORTB_0_we   => BRAM_PORTB_0_we_signal,     --(output) write enable signal
            BRAM_PORTB_0_dout => BRAM_PORTB_0_dout_signal,   --(input) data from ARM, will be sent to motors 
            BRAM_PORTB_1_addr => BRAM_PORTB_1_addr_signal,   --(output) address in BRAM to write to (sensor data)
            BRAM_PORTB_1_din  => BRAM_PORTB_1_din_signal,    --(output) data to BRAM, sensor data to be read by ARM received from local sensors   
            BRAM_PORTB_1_we   => BRAM_PORTB_1_we_signal,     --(output) write enable signal
            sensor_data     => sensor_data_signal,         --(input) data from ADC sensor
            motor_data      => motor_data_signal,          --(output) sends data to motor component or local motors when single node
            data_req_out    => data_req1,                  --(output) sends request to motor component to initiate 4-phase handshake 
            data_req_in     => data_req2,                  --(input) receives request from sensor component to initiate 4-phase handshake
            data_ack_in     => data_ack1,                  --(input) receives acknowledgement from receiver (motor)
            data_ack_out    => data_ack2,                  --(output) sends acknowledgement to sender (sensor)
            system_response_out => test_bit_signal,        --(output) updates bit flip
            interrupt_flag  => interrupt_flag_signal       --(output) triggers PL-PS interrupt, can be seen from the ARM
         );     

pwm_instance: pc_pwm_v11
    port map( 
           clk_50MHz        => clock_50M_signal,                                     
           data_req_pwm     => data_req1,
           data_ack_pwm     => data_ack1,
           motor_data_pwm   => motor_data_signal,                       
           pulse_out(0)     => pwm1_signal,
           pulse_out(1)     => pwm2_signal,
           pulse_out(2)     => pwm3_signal,
           pulse_out(3)     => pwm4_signal,
           pulse_out(4)     => pwm5_signal,
           pulse_out(5)     => pwm6_signal,
           pulse_out(6)     => pwm7_signal,
           pulse_out(7)     => pwm8_signal,
           pulse_out(8)     => pwm9_signal,
           pulse_out(9)     => pwm10_signal,
           pulse_out(10)    => pwm11_signal,
           pulse_out(11)    => pwm12_signal,
           pulse_out(12)    => pwm13_signal,
           pulse_out(13)    => pwm14_signal,
           pulse_out(14)    => pwm15_signal,
           pulse_out(15)    => pwm16_signal
    );

xadc_instance: xadc_v1
    port map(
        clk_125M => clock_125M,
        adc_vn14   => Vaux_v_n14,
        adc_vp14   => Vaux_v_p14,
        adc_vn7    => Vaux_v_n7,
        adc_vp7    => Vaux_v_p7,
        adc_vn15   => Vaux_v_n15,
        adc_vp15   => Vaux_v_p15,
        adc_vn6    => Vaux_v_n6,
        adc_vp6    => Vaux_v_p6,
        adc_out    => sensor_data_signal,
        data_req   => data_req2,
        data_ack   => data_ack2
    ); 
    
clock_instance: clock_50M_wrapper                                                                                                                                                                                
  Port map(            
        clk_out1_0 => clock_50M_signal,
        reset_rtl => '0',                                                                                                                                                                       
        sys_clock => clock_125M     --only place where 125M clock should be used, supplies the IP core which outputs 50MHz to all other components
    );

end Behavioral;