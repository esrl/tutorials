library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pc_pwm_v11 is
    Port ( 
       clk_50MHz        : in STD_LOGIC;
       data_req_pwm     : in STD_LOGIC;
       data_ack_pwm     : out STD_LOGIC;
       motor_data_pwm   : in STD_LOGIC_VECTOR (31 downto 0);
       pulse_out        : out STD_LOGIC_VECTOR (15 downto 0)
       );
end pc_pwm_v11;

architecture Behavioral of pc_pwm_v11 is

--pwm signals
signal pwm_counter : unsigned (19 downto 0) := x"00000";
signal ocr_counter : unsigned (19 downto 0) := x"00000";

--motor signals
type pwm_motor_array is array (0 to 15) of std_logic_vector(15 downto 0);
signal motor_data_int : STD_LOGIC_VECTOR (31 downto 0);

signal data_ack_int : STD_LOGIC;

--signals for prescaling main clock
signal prescale_clock : unsigned(31 downto 0) := x"00000000";
signal data_clock   : std_logic;

begin
 
     data_clock <= (prescale_clock(1));   
     
prescale: process(clk_50MHz)
begin
   if rising_edge(clk_50MHz) then
       prescale_clock <= prescale_clock + 1;
   end if;    
end process; 

read_motor_positions: process(data_req_pwm)     

variable data_req_flag_int : STD_LOGIC;
begin
    data_req_flag_int := data_req_pwm;
    if data_req_flag_int = '1' then
        data_ack_int <= '1';    
        motor_data_int <= motor_data_pwm; 
    elsif data_req_flag_int = '0' then
        data_ack_int <= '0';
    end if;                                 
end process;


pwm_counter_proc: process(clk_50MHz)
begin
if rising_edge(clk_50MHz) then

   if pwm_counter < 1000000 then  
       pwm_counter <= pwm_counter + 1;
       if pwm_counter < 500000 then ocr_counter <= ocr_counter + 1; 
       elsif pwm_counter >= 500000 then ocr_counter <= ocr_counter - 1; 
       else ocr_counter <= x"00000";
       end if;
   elsif pwm_counter >= 1000000 then pwm_counter <= x"00000"; ocr_counter <= x"00000";
   else pwm_counter <= x"00000"; ocr_counter <= x"00000";
   end if;
      
end if;
end process;

pwm_all_proc: process(clk_50MHz)
variable motor_data_array : pwm_motor_array  := (others =>x"0000");
variable motor_id_int : STD_LOGIC_VECTOR (7 downto 0) := x"01";
variable motor_pos_int : STD_LOGIC_VECTOR (15 downto 0) := x"0000";
begin
if rising_edge(clk_50MHz) then
        motor_id_int := motor_data_int(23 downto 16);
        motor_pos_int := motor_data_int(15 downto 0);
        motor_data_array(to_integer(unsigned(motor_id_int)-1)) := motor_pos_int;
        
        for i in 0 to 15 loop
        --signal high when counter <= OCR else signal low
        --for 16-bit, OCR = 37500 is equivalent to .75ms (minimum for old servos); 27200 is equivalent to .544ms (minimum for new servos)
            if ocr_counter <= (x"03520" + (unsigned(motor_data_array(i)) srl 1)) then          --minimum is 13600 since only counting on rising edge 
               pulse_out(i) <= '1';
            elsif ocr_counter > (x"03520" + (unsigned(motor_data_array(i)) srl 1)) then
               pulse_out(i) <= '0';
            else pulse_out(i) <= '0';
            end if;    
        end loop;
        
end if;    
end process;

output_process: process(clk_50MHz)
begin
    if rising_edge(clk_50MHz) then
        data_ack_pwm <= data_ack_int;
    end if;
end process;
end Behavioral;
