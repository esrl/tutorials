library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity main_mem_v7 is
    Port ( 
        clock           : in STD_LOGIC;
    BRAM_PORTB_0_addr : out STD_LOGIC_VECTOR ( 31 downto 0 );              
    BRAM_PORTB_0_we   : out STD_LOGIC_VECTOR ( 3 downto 0 );
    BRAM_PORTB_0_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_1_addr : out STD_LOGIC_VECTOR ( 31 downto 0 );        
    BRAM_PORTB_1_din  : out STD_LOGIC_VECTOR ( 31 downto 0 );       
    BRAM_PORTB_1_we   : out STD_LOGIC_VECTOR ( 3 downto 0 );
    sensor_data     : in STD_LOGIC_VECTOR ( 31 downto 0 );
    motor_data      : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data_req_out    : out STD_LOGIC;
    data_req_in     : in STD_LOGIC;
    data_ack_in     : in STD_LOGIC;
    data_ack_out    : out STD_LOGIC;
    system_response_out : out STD_LOGIC;
    interrupt_flag  : out STD_LOGIC
    );
end main_mem_v7;

architecture Behavioral of main_mem_v7 is

--define states for state machine
type wmemory_state_type is (wmemory_wait, wmemory_update_address, wmemory_write_data);
type rmemory_state_type is (rmemory_wait, rmemory_update_address, rmemory_read_data);
signal wmemory_state, wmemory_next_state : wmemory_state_type;
signal rmemory_state, rmemory_next_state  : rmemory_state_type;

--signals for prescaling main clock
signal prescale_clock : unsigned(31 downto 0) := x"00000000";
signal memory_clock   : std_logic;

--signals for output
signal wmemory_address, rmemory_address : STD_LOGIC_VECTOR (31 downto 0);
signal wwrite_enable, rwrite_enable     : STD_LOGIC_VECTOR (3 downto 0);
signal interrupt_trigger   : STD_LOGIC := '0';
signal interrupt_read, interrupt_write : STD_LOGIC := '0';
signal data_to_memory   : STD_LOGIC_VECTOR (31 downto 0);
signal motor_data_out   : STD_LOGIC_VECTOR (31 downto 0);
signal data_req_flag, data_ack_flag    : STD_LOGIC;
signal msb_local    : STD_LOGIC_VECTOR(1 downto 0) := "00";
signal data_read_internal : STD_LOGIC := '0';
signal system_response : STD_LOGIC := '0';

begin
 
     memory_clock <= (prescale_clock(1));   
     
prescale: process(clock)
begin
   if rising_edge(clock) then
       prescale_clock <= prescale_clock + 1;
   end if;    
end process; 

NEXT_STATE_PROC: process (clock)    
begin
    if rising_edge(clock) then
        wmemory_state <= wmemory_next_state;
        rmemory_state <= rmemory_next_state;
    end if;
end process;

SYNC_PROC_READ: process (memory_clock)
     
 --variables for main memory 
 variable memory_current_address : unsigned(31 downto 0) := x"00000000";   --initialize address to first overall memory slot
 variable memory_increment       : unsigned(3 downto 0) := "0000";                            
 variable data_ack_flag_int      : STD_LOGIC := '0';
 variable data_req_flag_int      : STD_LOGIC := '0';
 variable motor_msb_int    : STD_LOGIC_VECTOR (0 downto 0) := "0"; 
 
 begin
 if rising_edge(memory_clock) then    
   
   case(rmemory_state) is
   when rmemory_wait => rwrite_enable <= "0000";
                        if interrupt_trigger = '1' then interrupt_read <= '0'; end if;
                        data_req_flag <= '1';
                        data_ack_flag_int := data_ack_in;
                        if data_ack_flag_int = '1' then              --ADD A TIMEOUT
                           data_req_flag <= '0';  
                           rmemory_next_state <= rmemory_update_address;
                        end if;         
   when rmemory_update_address =>
    if memory_increment >= 15 then                            --reset address and increment value when all motor addresses checked (16 motor outputs per node)
           memory_current_address := x"00000000"; 
           memory_increment := "0000";
           interrupt_read <= '1';
    else
           memory_current_address := memory_current_address + 4;         --increment to next immediate address
           memory_increment := memory_increment + 1;
    end if;

    rmemory_address <= std_logic_vector(memory_current_address);
    rmemory_next_state <= rmemory_read_data;                         
        
   when rmemory_read_data =>     
                          rwrite_enable <= "0000";
                          motor_data_out <= BRAM_PORTB_0_dout;
                          motor_msb_int := BRAM_PORTB_0_dout(31 downto 31);
                          if motor_msb_int = "1" then system_response <= system_response xor '1'; end if;             --system speed test bit inverter  
                          rmemory_next_state <= rmemory_wait;
   
   when others =>  rwrite_enable <= "0000";
                   interrupt_read <= '0';        
   end case;

 end if;
end process;

SYNC_PROC_WRITE: process (memory_clock)
     
 --variables for main memory 
 variable memory_current_address : unsigned(31 downto 0) := x"00001000";   --initialize address to first memory slot of second half of memory block
 variable memory_increment       : unsigned(2 downto 0) := "000";          
 variable read_write             : STD_LOGIC := '1';                        
 variable data_ack_flag_int      : STD_LOGIC := '0';
 variable data_req_flag_int      : STD_LOGIC := '0';
 variable msb_update             : STD_LOGIC_VECTOR(1 downto 0) := "00";
 
 begin
 if rising_edge(memory_clock) then    
   
   case(wmemory_state) is
   when wmemory_wait => wwrite_enable <= "0000";
                       if interrupt_trigger = '1' then interrupt_write <= '0'; end if;
                       wmemory_next_state <= wmemory_update_address;        
   
   when wmemory_update_address =>
    if memory_increment >= 3 then                            --reset address and increment value when all sensor data written (4 sensors per node)
          memory_current_address := x"00001000";             --sensor data stored in addresses 1024-2047 
          memory_increment := "000";
          interrupt_write <= '1';
    else
          memory_current_address := memory_current_address + 4;         --increment to next immediate address
          memory_increment := memory_increment + 1;
    end if;
    
    if msb_update = "01" then
        data_read_internal <= data_read_internal xor '1';    
    end if;

   wmemory_address <= std_logic_vector(memory_current_address); 
   wmemory_next_state <= wmemory_write_data;                       
   when wmemory_write_data => 
                            wwrite_enable <= "1111"; 
                            data_req_flag_int := data_req_in;           --ADD TIMEOUT
                            if data_req_flag_int = '1' then
                                data_ack_flag <= '1'; 
                                msb_update := msb_local;   
                                data_to_memory <= msb_update & sensor_data(29 downto 0);
                            elsif data_req_flag_int = '0' then
                                data_ack_flag <= '0';     
                                wmemory_next_state <= wmemory_wait;
                            end if;         
   
   when others =>  wwrite_enable <= "0000";
                   interrupt_write <= '0';                         
   end case;

 end if;
end process;

INTERRUPT_CHECK: process (interrupt_write,interrupt_read)
begin

    interrupt_trigger <= interrupt_read AND interrupt_write;

end process;

OUTPUT_DECODE: process (memory_clock)
begin
if rising_edge(memory_clock) then
     BRAM_PORTB_0_addr <= rmemory_address;      
     BRAM_PORTB_0_we   <= rwrite_enable;
     BRAM_PORTB_1_addr <= wmemory_address;      
     BRAM_PORTB_1_we   <= wwrite_enable;
     interrupt_flag  <= interrupt_trigger;
     BRAM_PORTB_1_din  <= data_to_memory;
     motor_data      <= motor_data_out;    
     data_req_out    <= data_req_flag;
     data_ack_out    <= data_ack_flag;
     system_response_out <= system_response;        
end if;
end process;

bit_flip_local: process(system_response,data_read_internal)
variable system_response_current_loc, system_response_old_loc : std_logic := '0'; 
variable internal_response_current, internal_response_old : std_logic := '0';
begin

    system_response_current_loc := system_response;
    if system_response_current_loc = NOT system_response_old_loc then 
        system_response_old_loc := system_response_current_loc;
        msb_local <= "01";
    end if;
    
    internal_response_current := data_read_internal;        
    if (internal_response_current = NOT internal_response_old) then 
        msb_local <= "00"; 
        internal_response_old := internal_response_current;
    end if;  
end process;

end Behavioral;
