## Constraints file for memory interface

##Clock signal
set_property -dict {PACKAGE_PIN K17 IOSTANDARD LVCMOS33} [get_ports clock_125M]
create_clock -period 8.000 -name sys_clk_pin -waveform {0.000 4.000} -add [get_ports clock_125M]

#set_property -dict {PACKAGE_PIN K17 IOSTANDARD LVCMOS33} [get_ports clock_50M]
#create_clock -period 20.000 -name sys_clk_pin -waveform {0.000 10.000} -add [get_ports clock_50M]   #50MHz has clock period of 20ns, waveform gives rising and falling edge (0 to half period)

##Pmod Header JA (XADC)
set_property -dict {PACKAGE_PIN N15 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p14]
set_property -dict {PACKAGE_PIN L14 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p7]
set_property -dict {PACKAGE_PIN K16 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p15]
set_property -dict {PACKAGE_PIN K14 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p6]
set_property -dict {PACKAGE_PIN N16 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n14]
set_property -dict {PACKAGE_PIN L15 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n7]
set_property -dict {PACKAGE_PIN J16 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n15]
set_property -dict {PACKAGE_PIN J14 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n6]

##Pmod Header JC
#set_property -dict {PACKAGE_PIN V15 IOSTANDARD LVCMOS33} [get_ports sig_in]
#set_property -dict { PACKAGE_PIN W15   IOSTANDARD LVCMOS33     } [get_ports { jc[1] }]; #IO_L10N_T1_34 Sch=jc_n[1]
#set_property -dict { PACKAGE_PIN T11   IOSTANDARD LVCMOS33     } [get_ports { jc[2] }]; #IO_L1P_T0_34 Sch=jc_p[2]
#set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33     } [get_ports { jc[3] }]; #IO_L1N_T0_34 Sch=jc_n[2]
#set_property -dict {PACKAGE_PIN W14 IOSTANDARD LVCMOS33} [get_ports sig_out]
#set_property -dict { PACKAGE_PIN Y14   IOSTANDARD LVCMOS33     } [get_ports { jc[5] }]; #IO_L8N_T1_34 Sch=jc_n[3]
#set_property -dict { PACKAGE_PIN T12   IOSTANDARD LVCMOS33     } [get_ports { jc[6] }]; #IO_L2P_T0_34 Sch=jc_p[4]
#set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33     } [get_ports { jc[7] }]; #IO_L2N_T0_34 Sch=jc_n[4]

##Pmod Header JC (Motors)
set_property -dict {PACKAGE_PIN V15 IOSTANDARD LVCMOS33} [get_ports {motor_out[0]}]
set_property -dict {PACKAGE_PIN W15 IOSTANDARD LVCMOS33} [get_ports {motor_out[1]}]
set_property -dict {PACKAGE_PIN T11 IOSTANDARD LVCMOS33} [get_ports {motor_out[2]}]
set_property -dict {PACKAGE_PIN T10 IOSTANDARD LVCMOS33} [get_ports {motor_out[3]}]
set_property -dict {PACKAGE_PIN W14 IOSTANDARD LVCMOS33} [get_ports {motor_out[4]}]
set_property -dict {PACKAGE_PIN Y14 IOSTANDARD LVCMOS33} [get_ports {motor_out[5]}]
set_property -dict {PACKAGE_PIN T12 IOSTANDARD LVCMOS33} [get_ports {motor_out[6]}]
set_property -dict {PACKAGE_PIN U12 IOSTANDARD LVCMOS33} [get_ports {motor_out[7]}]

##Pmod Header JD (Motors)
set_property -dict {PACKAGE_PIN T14 IOSTANDARD LVCMOS33} [get_ports {motor_out[8]}]
set_property -dict {PACKAGE_PIN T15 IOSTANDARD LVCMOS33} [get_ports {motor_out[9]}]
set_property -dict {PACKAGE_PIN P14 IOSTANDARD LVCMOS33} [get_ports {motor_out[10]}]
set_property -dict {PACKAGE_PIN R14 IOSTANDARD LVCMOS33} [get_ports {motor_out[11]}]
set_property -dict {PACKAGE_PIN U14 IOSTANDARD LVCMOS33} [get_ports {motor_out[12]}]
set_property -dict {PACKAGE_PIN U15 IOSTANDARD LVCMOS33} [get_ports {motor_out[13]}]
set_property -dict {PACKAGE_PIN V17 IOSTANDARD LVCMOS33} [get_ports {motor_out[14]}]
set_property -dict {PACKAGE_PIN V18 IOSTANDARD LVCMOS33} [get_ports {motor_out[15]}]

##OLD CONSTRAINTS (only JD used for testing originally)
#set_property -dict {PACKAGE_PIN T14 IOSTANDARD LVCMOS33} [get_ports sig_in]
#set_property -dict {PACKAGE_PIN T15 IOSTANDARD LVCMOS33} [get_ports test_out]   #IO_L5N_T0_34 Sch=jd_n[1]
#set_property -dict {PACKAGE_PIN P14 IOSTANDARD LVCMOS33} [get_ports motor_out]
#set_property -dict {PACKAGE_PIN U14 IOSTANDARD LVCMOS33} [get_ports sig_out]

##Pmod Header JE (Motors)
#set_property -dict {PACKAGE_PIN V12 IOSTANDARD LVCMOS33} [get_ports {motor_out[8]}]
#set_property -dict {PACKAGE_PIN W16 IOSTANDARD LVCMOS33} [get_ports {motor_out[9]}]
#set_property -dict {PACKAGE_PIN J15 IOSTANDARD LVCMOS33} [get_ports {motor_out[10]}]
#set_property -dict {PACKAGE_PIN H15 IOSTANDARD LVCMOS33} [get_ports {motor_out[11]}]
#set_property -dict {PACKAGE_PIN V13 IOSTANDARD LVCMOS33} [get_ports {motor_out[12]}]
#set_property -dict {PACKAGE_PIN U17 IOSTANDARD LVCMOS33} [get_ports {motor_out[13]}]
#set_property -dict {PACKAGE_PIN T17 IOSTANDARD LVCMOS33} [get_ports {motor_out[14]}]
#set_property -dict {PACKAGE_PIN Y17 IOSTANDARD LVCMOS33} [get_ports {motor_out[15]}]

##Pmod Header JE (TosNet)
set_property -dict {PACKAGE_PIN V12 IOSTANDARD LVCMOS33} [get_ports sig_out]
set_property -dict {PACKAGE_PIN W16 IOSTANDARD LVCMOS33} [get_ports system_test_bit]
set_property -dict {PACKAGE_PIN J15 IOSTANDARD LVCMOS33} [get_ports system_test_bit_tos]
#set_property -dict {PACKAGE_PIN H15 IOSTANDARD LVCMOS33} [get_ports {motor_out[11]}]
set_property -dict {PACKAGE_PIN V13 IOSTANDARD LVCMOS33} [get_ports sig_in]
#set_property -dict {PACKAGE_PIN U17 IOSTANDARD LVCMOS33} [get_ports {motor_out[13]}]
#set_property -dict {PACKAGE_PIN T17 IOSTANDARD LVCMOS33} [get_ports {motor_out[14]}]
#set_property -dict {PACKAGE_PIN Y17 IOSTANDARD LVCMOS33} [get_ports {motor_out[15]}]

##LEDs
set_property -dict {PACKAGE_PIN M14 IOSTANDARD LVCMOS33} [get_ports {led_indicator[0]}]
set_property -dict {PACKAGE_PIN M15 IOSTANDARD LVCMOS33} [get_ports {led_indicator[1]}]
set_property -dict {PACKAGE_PIN G14 IOSTANDARD LVCMOS33} [get_ports {led_indicator[2]}]
set_property -dict {PACKAGE_PIN D18 IOSTANDARD LVCMOS33} [get_ports {led_indicator[3]}]

##Switches
set_property -dict {PACKAGE_PIN G15 IOSTANDARD LVCMOS33} [get_ports {sw_setting[0]}]
set_property -dict {PACKAGE_PIN P15 IOSTANDARD LVCMOS33} [get_ports {sw_setting[1]}]
set_property -dict {PACKAGE_PIN W13 IOSTANDARD LVCMOS33} [get_ports {sw_setting[2]}]
set_property -dict {PACKAGE_PIN T16 IOSTANDARD LVCMOS33} [get_ports {sw_setting[3]}]

#create_debug_core u_ila_0 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
#set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
#set_property port_width 1 [get_debug_ports u_ila_0/clk]
#connect_debug_port u_ila_0/clk [get_nets [list clock_instance/clock_50M_i/clk_wiz_0/inst/clk_out1]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
#set_property port_width 16 [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {motor_out_OBUF[0]} {motor_out_OBUF[1]} {motor_out_OBUF[2]} {motor_out_OBUF[3]} {motor_out_OBUF[4]} {motor_out_OBUF[5]} {motor_out_OBUF[6]} {motor_out_OBUF[7]} {motor_out_OBUF[8]} {motor_out_OBUF[9]} {motor_out_OBUF[10]} {motor_out_OBUF[11]} {motor_out_OBUF[12]} {motor_out_OBUF[13]} {motor_out_OBUF[14]} {motor_out_OBUF[15]}]]
#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets clock_50M_signal]

create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list main_mem_instance/memory_clock_BUFG]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 32 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {BRAM_PORTB_0_addr_signal[0]} {BRAM_PORTB_0_addr_signal[1]} {BRAM_PORTB_0_addr_signal[2]} {BRAM_PORTB_0_addr_signal[3]} {BRAM_PORTB_0_addr_signal[4]} {BRAM_PORTB_0_addr_signal[5]} {BRAM_PORTB_0_addr_signal[6]} {BRAM_PORTB_0_addr_signal[7]} {BRAM_PORTB_0_addr_signal[8]} {BRAM_PORTB_0_addr_signal[9]} {BRAM_PORTB_0_addr_signal[10]} {BRAM_PORTB_0_addr_signal[11]} {BRAM_PORTB_0_addr_signal[12]} {BRAM_PORTB_0_addr_signal[13]} {BRAM_PORTB_0_addr_signal[14]} {BRAM_PORTB_0_addr_signal[15]} {BRAM_PORTB_0_addr_signal[16]} {BRAM_PORTB_0_addr_signal[17]} {BRAM_PORTB_0_addr_signal[18]} {BRAM_PORTB_0_addr_signal[19]} {BRAM_PORTB_0_addr_signal[20]} {BRAM_PORTB_0_addr_signal[21]} {BRAM_PORTB_0_addr_signal[22]} {BRAM_PORTB_0_addr_signal[23]} {BRAM_PORTB_0_addr_signal[24]} {BRAM_PORTB_0_addr_signal[25]} {BRAM_PORTB_0_addr_signal[26]} {BRAM_PORTB_0_addr_signal[27]} {BRAM_PORTB_0_addr_signal[28]} {BRAM_PORTB_0_addr_signal[29]} {BRAM_PORTB_0_addr_signal[30]} {BRAM_PORTB_0_addr_signal[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 32 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {motor_data_signal[0]} {motor_data_signal[1]} {motor_data_signal[2]} {motor_data_signal[3]} {motor_data_signal[4]} {motor_data_signal[5]} {motor_data_signal[6]} {motor_data_signal[7]} {motor_data_signal[8]} {motor_data_signal[9]} {motor_data_signal[10]} {motor_data_signal[11]} {motor_data_signal[12]} {motor_data_signal[13]} {motor_data_signal[14]} {motor_data_signal[15]} {motor_data_signal[16]} {motor_data_signal[17]} {motor_data_signal[18]} {motor_data_signal[19]} {motor_data_signal[20]} {motor_data_signal[21]} {motor_data_signal[22]} {motor_data_signal[23]} {motor_data_signal[24]} {motor_data_signal[25]} {motor_data_signal[26]} {motor_data_signal[27]} {motor_data_signal[28]} {motor_data_signal[29]} {motor_data_signal[30]} {motor_data_signal[31]}]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets u_ila_0_memory_clock_BUFG]
