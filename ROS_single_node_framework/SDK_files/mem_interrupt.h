#ifndef SRC_MEM_INTERRUPT_H_
#define SRC_MEM_INTERRUPT_H_

/***************************** Include Files *********************************/

#include "xparameters.h"
#include "xil_exception.h"
#include "xscugic.h"
#include "xil_printf.h"

/************************** Constant Definitions *****************************/

#define MEMINTR_DEVICE_ID	XPAR_PS7_SCUGIC_0_DEVICE_ID //not used
#define INTC_INTERRUPT_ID	XPAR_FABRIC_WRAPPER_INSTANCE_SYSTEM_I_IRQ_INTR //XPAR_FABRIC_IRQ_INTR //XPAR_FABRIC_SYSTEM_IRQ_INTERRUPT_INTR
#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID
#define INTC			XScuGic
#define INTC_HANDLER	XScuGic_InterruptHandler

/************************** Function Prototypes ******************************/
int initInterrupt(void);
void memIntrHandler(void *CallBackRef);

int SetupIntrSystem(INTC *IntcInstancePtr, u16 IntrId);

/************************** Variable Definitions *****************************/
INTC Intc; /* The Instance of the Interrupt Controller Driver */

//static volatile u32 IntrFlag; /* Interrupt Handler Flag */
uint32_t intrFlag;
extern uint32_t intrCounter;

#endif /* SRC_MEM_INTERRUPT_H_ */
