/*
 * Copyright (c) 2009 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


/***************************** Include Files *********************************/
#include "xparameters.h"
#include "xil_types.h"
//#include <stdio.h>
//#include <stdlib.h>
#include <unistd.h>
//#include "xil_io.h"
//#include "xil_mmu.h"
//#include "xil_cache.h"
//#include "xil_printf.h"
//#include "xil_exception.h"
#include "xscugic.h"
//#include "xintc.h"
////#include "sleep.h"
//#include "scu_sleep.h"
//#include "xpseudo_asm.h"

/**************************** Type Definitions *******************************/

/************************** Constant Definitions *****************************/

/*
 * Use SCU timer instead of global timer for 1sec sleep
 * NOTE: The SCU timer is used for profiling so do not
 * enable both profiling and USE_SCU_TIMER
 */
#define USE_SCU_TIMER


/**************************** Type Definitions *******************************/
/**
 * This typedef contains configuration information for the device driver.
 */
typedef struct {
	u16 DeviceId;		/**< Unique ID of device */
	u32 BaseAddress;	/**< Base address of the device */
} Pl_Config;


/**
 * The driver instance data. The user is required to allocate a
 * variable of this type.
 * A pointer to a variable of this type is then passed to the driver API
 * functions.
 */
typedef struct {
	Pl_Config Config;   /**< Hardware Configuration */
	u32 IsReady;		/**< Device is initialized and ready */
	u32 IsStarted;		/**< Device is running */
} XPlIrq;


/***************** Macros (Inline Functions) Definitions *********************/
#define COUNTER         (*(volatile unsigned long *)(0xFFFF8000))
#define SENSOR_ID		(*(volatile unsigned long *)(0xFFFF8004))
#define SENSOR_DATA		(*(volatile unsigned long *)(0xFFFF8008))
#define SYNC_FLAG		(*(volatile unsigned long *)(0xFFFF8020))
#define SYSTEM_TEST		(*(volatile unsigned long *)(0xFFFF8024))
#define SYSTEM_TEST_RESPONSE (*(volatile unsigned long *)(0xFFFF8028))
#define INTR_COUNTER	(*(volatile unsigned long *)(0xFFFF802C))
#define INTR_FLAG		(*(volatile unsigned long *)(0xFFFF8030))

#define COMM_TX_FLAG    (*(volatile unsigned long *)(0xFFFF9000))
#define COMM_TX_DATA    (*(volatile unsigned long *)(0xFFFF9004))
#define COMM_RX_FLAG    (*(volatile unsigned long *)(0xFFFF9008))
#define COMM_RX_DATA    (*(volatile unsigned long *)(0xFFFF900C))


/************************** Function Prototypes ******************************/

/************************** Variable Definitions *****************************/

