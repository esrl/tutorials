#include "mem_interrupt.h"

uint32_t intrCounter = 0;

int initInterrupt(void)
{
	int Status;

	  //xil_printf("Initializing interrupts...\r\n");

	  Status = SetupIntrSystem(&Intc,
				   INTC_INTERRUPT_ID);
	  if (Status != XST_SUCCESS) {
	  		return XST_FAILURE;
	  	}

	  //xil_printf("Done.\r\n");

	return XST_SUCCESS;
}

int SetupIntrSystem(INTC *IntcInstancePtr, u16 IntrId)
{
	int Result;

	XScuGic_Config *IntcConfig;

	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

	Result = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Result != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XScuGic_SetPriorityTriggerType(IntcInstancePtr, IntrId,
					0xA0, 0x3);

	Result = XScuGic_Connect(IntcInstancePtr, IntrId,
				 (Xil_ExceptionHandler)memIntrHandler, (void *)&IntcInstancePtr);	//InstancePtr);
	if (Result != XST_SUCCESS) {
		return Result;
	}

	XScuGic_InterruptMaptoCpu(IntcInstancePtr, XPAR_CPU_ID, IntrId);	//interrupt pointed to CPU1

	XScuGic_Enable(IntcInstancePtr, IntrId);

	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			 (Xil_ExceptionHandler)INTC_HANDLER, IntcInstancePtr);
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}

void memIntrHandler(void *CallbackRef)
{
	//IntrFlag = 1;							//keep this variable - use later
	/*if (intrCounter < 50)
	{
		intrCounter++;
	}
	else
	{
		intrCounter = 0;
		xil_printf("PL sent 50 interrupts.\n\r");
	}*/
	intrFlag = 1;
	intrCounter++;			//count up each time program is interrupted

}
