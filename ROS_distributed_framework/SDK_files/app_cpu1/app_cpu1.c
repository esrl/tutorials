/*License:     Copyright (C) 2019  Beck Strohmer

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#include "xparameters.h"
#include "app_cpu1.h"
#include "my_utils.h"
#include "bram.h"
#include "mem_interrupt.h"

#ifdef USE_SCU_TIMER
#include "scu_sleep.h"
#else
#include "sleep.h"
#endif


int main()
{
	initMemory();
	initInterrupt();

	// Disable L1 cache for OCM
	MyXil_SetTlbAttributes(0xFFFF0000,0x04de2);           // S=b0 TEX=b100 AP=b11, Domain=b1111, C=b0, B=b0

	uint32_t write_value, prev_write_value = 0;
    uint32_t write_id_mask = 0x00FF0000;
    uint32_t write_value_id=0;

    uint32_t sensor_id_mask = 0x3F000000;
	uint32_t sensor_data_mask = 0x00000FFF;
	uint32_t sensor_id, sensor_data, requested_id;

	uint32_t bare_metal_flag_mask = 0x80000000;			//set MSB high to send system test bit to FPGA
	uint32_t sys_resp_flag_mask = 0x40000000;			//check if next MSB high for response
	uint32_t msb_address, msb_value, msb_flag = 0;
	uint32_t response_id = 0;

    COUNTER=0;

	/*
	 * Initialize the scu timer to be used for sleep
	 */
	#ifdef USE_SCU_TIMER
		ScuTimerInit();
	#endif

	int32_t sensor_start_address = 1024;
	int32_t sensor_end_address = 1024+64;

	SYNC_FLAG = 1;				//initialize flag to tell ROS that program is ready
	SYSTEM_TEST = 0;

  while(1)
  {
      COUNTER+=1;					//check CPU1 is running
      INTR_FLAG = intrFlag;

      if(SYSTEM_TEST_RESPONSE == 0)
      {
		  for(int address=sensor_start_address; address<sensor_end_address;address++)
		  {
			  if ((sys_resp_flag_mask & MYMEM_u(address)) == sys_resp_flag_mask)
			  {
				  response_id = (sensor_id_mask & MYMEM_u(address)) >> 24;
				  /*if (response_id >=1 && response_id <=4) SYSTEM_TEST_RESPONSE = 1;
				  else if (response_id >=5 && response_id <=8) SYSTEM_TEST_RESPONSE = 2;
				  else if (response_id >=9 && response_id <=12) SYSTEM_TEST_RESPONSE = 3;
				  else if (response_id >=13 && response_id <=16) SYSTEM_TEST_RESPONSE = 4;
				  else if (response_id >=17 && response_id <=20) SYSTEM_TEST_RESPONSE = 5;
				  else if (response_id >=21 && response_id <=24) SYSTEM_TEST_RESPONSE = 6;
				  else if (response_id >=25 && response_id <=28) SYSTEM_TEST_RESPONSE = 7;
				  else if (response_id >=29 && response_id <=32) SYSTEM_TEST_RESPONSE = 8;
				  else if (response_id >=33 && response_id <=36) SYSTEM_TEST_RESPONSE = 9;
				  else if (response_id >=37 && response_id <=40) SYSTEM_TEST_RESPONSE = 10;
				  else if (response_id >=41 && response_id <=44) SYSTEM_TEST_RESPONSE = 11;
				  else if (response_id >=45 && response_id <=48) SYSTEM_TEST_RESPONSE = 12;
				  else if (response_id >=49 && response_id <=52) SYSTEM_TEST_RESPONSE = 13;
				  else if (response_id >=53 && response_id <=56) SYSTEM_TEST_RESPONSE = 14;
				  else if (response_id >=57 && response_id <=60) SYSTEM_TEST_RESPONSE = 15;*/
				  if (response_id >=13 && response_id <=16)
				  {
					  continue;
				  }
				  if (response_id >=9 && response_id <=12)
				  {
					  SYSTEM_TEST_RESPONSE = 3;
				  	  MYMEM_u(address) = 0x3FFFFFFF & MYMEM_u(address);		//set MSB low again for the address
				  	  break;
				  }
			  }
		  }
      }

	  if(SYSTEM_TEST_RESPONSE_2 == 0)
	  {
		  for(int address=sensor_start_address; address<sensor_end_address;address++)
		  {
			  if ((sys_resp_flag_mask & MYMEM_u(address)) == sys_resp_flag_mask)
			  {
				  response_id = (sensor_id_mask & MYMEM_u(address)) >> 24;
				  if (response_id >=9 && response_id <=12)
				  {
					  continue;
				  }
				  if (response_id >=13 && response_id <=16)
				  {
					  SYSTEM_TEST_RESPONSE_2 = 4;
				  	  MYMEM_u(address) = 0x3FFFFFFF & MYMEM_u(address);		//set MSB low again for the address
				  	  break;
				  }
			  }
		    }
		}



	  //if (intrFlag == 1)							//TESTING - WITHOUT ROS
      if ((SYNC_FLAG == 0) & (intrFlag == 1))
      {

		  /*Memory interface write*/
    	  intrFlag = 0;	 									//reset interrupt flag
		  write_value = (*(volatile u32*)0xFFFF8010);		//update value to write to BRAM
		  write_value_id = ((write_id_mask & (*(volatile u32*)0xFFFF8010)) >> 16);
		  if (SYSTEM_TEST == 1) {write_value = bare_metal_flag_mask | write_value; SYSTEM_TEST = 0;}	//set MSB high if flag received from ROS

		  /*if (msb_flag == 1)
		  {
			  MYMEM_u(msb_address)= (msb_value & 0x00FFFFFF);	//clear previous MSB
			  msb_flag = 0;
		  }*/

		  if (write_value != prev_write_value)
		  {
			  prev_write_value = write_value;

			  MYMEM_u(write_value_id-1)=write_value;
			  if ((bare_metal_flag_mask & write_value) == bare_metal_flag_mask)
			  {
				  msb_address = write_value_id-1;
				  msb_value = write_value;
				  msb_flag = 1;
			  }
		  }
		  /*Memory interface read*/
		  requested_id = (*(volatile u32*)0xFFFF800C);	//use value from CPU0 to select sensor
		  if(requested_id>0 && requested_id<=60)
		  {
			  for(int address=sensor_start_address; address<sensor_end_address;address++)
			  {
				  sensor_id = (sensor_id_mask & MYMEM_u(address)) >> 24;
				  if (requested_id == sensor_id)
				  {
					  sensor_data = (sensor_data_mask & MYMEM_u(address));
					  SENSOR_ID = sensor_id;
					  SENSOR_DATA = sensor_data;	//located on OCM address 0xFFFF8008
					  (*(volatile u32*)0xFFFF800C) = 0;
					  break;
				  }

			  }
		  }
		  /*Confirm MSB read and clear*/
		  if (msb_flag == 1)
		  {
			  while (intrFlag != 1) {usleep(1);}
			  MYMEM_u(msb_address)= (msb_value & 0x00FFFFFF);	//clear previous MSB
			  msb_flag = 0;
		  }

	  SYNC_FLAG = 1;
	  //sleep(1);
      }	//end if statement protected by interrupt

  } //end while loop

	return 0;
}  //end main function

