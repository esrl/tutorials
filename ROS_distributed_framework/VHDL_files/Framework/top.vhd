----------------------------------------------------------------------------------
-- Company: University of Southern Denmark
-- Engineer: Beck Strohmer
-- 
-- Create Date: 02/21/2018 10:19:21 AM
-- Design Name: Memory Interface
-- Module Name: top - Behavioral
-- Project Name: Flexible High-Performance Hardware Framework for Modular Experimental Robotics
-- Target Devices: ZYBO 
-- Tool Versions: Vivado 2017.4
-- Description: Single node function (main memory interface)
--              Networked node function (main and distributed memory interfaces)
--              Component descriptions:
--              system_wrapper: local BRAM for communication with the bare-metal core
--              main_mem_v7: interface for communication between memory layer and software layer
--              distributed_memory_v6: interface for communication between memory layer (TosNet) and I/O layer
--              motor_data_v3: motor data buffer between main memory interface and master distributed memory interface
--              pc_pwm_v10: PWM generator, I/O layer component
--              xadc: analog input translation, I/O layer component
--              tosnet: TosNet framework for mirroring memory across a distributed network of FPGAs
--              tosnet_clock_wrapper: 50MHz clock output
--              debounce: debounce component for switches
--              set_tosnet_id: set Node ID for FPGA when operating as networked nodes    
-- 
-- Dependencies: 
-- 
-- Revision: 2
-- Revision 0.02 - Single and networked nodes implemented
--                 PWM component for control of servo motors
--                 Input sensors through XADC PMOD
--                 4-phase handshake implemented throughout
--                 Distributed memory synched to TosNet
-- Additional Comments:
-- TosNet files imported from https://opencores.org/projects/tosnet

-- License:     Copyright (C) 2019  Beck Strohmer

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.motors.ALL;

entity top is
    Port ( 
        clock_125M      : in STD_LOGIC;
        Vaux_v_n14      : in STD_LOGIC;
        Vaux_v_p14      : in STD_LOGIC;
        Vaux_v_n7       : in STD_LOGIC;
        Vaux_v_p7       : in STD_LOGIC;
        Vaux_v_n15      : in STD_LOGIC;
        Vaux_v_p15      : in STD_LOGIC;
        Vaux_v_n6       : in STD_LOGIC;
        Vaux_v_p6       : in STD_LOGIC;
        sw_setting      : in STD_LOGIC_VECTOR ( 3 downto 0 );       --Switches enable TosNet and assign Node ID
        motor_out       : out STD_LOGIC_VECTOR ( 15 downto 0 );     --PWM signal to servos (JC & JD)
        led_indicator   : out STD_LOGIC_VECTOR ( 3 downto 0 );
        sig_in          : in STD_LOGIC;                             --to TosNet receiver (JE0)
        system_test_bit : out STD_LOGIC;                            --system speed test bit
        system_test_bit_tos : out STD_LOGIC;                        
        sig_out         : inout STD_LOGIC                           --to TosNet transmitter (JE7) 
    );
end top;

architecture Behavioral of top is

signal BRAM_PORTB_0_addr_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_raddr_signal  : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_clk_signal    : STD_LOGIC;
signal BRAM_PORTB_0_din_signal    : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_dout_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_0_en_signal     : STD_LOGIC;
signal BRAM_PORTB_0_rst_signal    : STD_LOGIC;
signal BRAM_PORTB_0_we_signal     : STD_LOGIC_VECTOR ( 3 downto 0 );
signal BRAM_PORTB_1_addr_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_raddr_signal  : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_clk_signal    : STD_LOGIC;
signal BRAM_PORTB_1_din_signal    : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_dout_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal BRAM_PORTB_1_en_signal     : STD_LOGIC;
signal BRAM_PORTB_1_rst_signal    : STD_LOGIC;
signal BRAM_PORTB_1_we_signal     : STD_LOGIC_VECTOR ( 3 downto 0 );
signal interrupt_flag_signal    : STD_LOGIC := '0';
signal sensor_data_signal       : STD_LOGIC_VECTOR ( 31 downto 0 );

signal motor_data_array_signal  : motor_array;      

--TosNet signals
signal sync_strobe_signal       : STD_LOGIC;
signal online_signal            : STD_LOGIC;
signal is_master_signal         : STD_LOGIC;
signal packet_error_signal      : STD_LOGIC;
signal system_halt_signal       : STD_LOGIC;
signal data_reg_addr_signal     : STD_LOGIC_VECTOR(9 downto 0) := "0000000000";    
signal data_reg_data_in_signal  : STD_LOGIC_VECTOR(31 downto 0);
signal data_reg_data_out_signal : STD_LOGIC_VECTOR(31 downto 0);
signal data_reg_clk_signal      : STD_LOGIC;
signal data_reg_we_signal       : STD_LOGIC_VECTOR(0 downto 0);
signal commit_write_signal      : STD_LOGIC;
signal commit_read_signal       : STD_LOGIC;
signal reset_counter_signal     : STD_LOGIC_VECTOR(31 downto 0);
signal packet_counter_signal    : STD_LOGIC_VECTOR(31 downto 0);
signal error_counter_signal     : STD_LOGIC_VECTOR(31 downto 0);
signal async_in_data_signal     : STD_LOGIC_VECTOR(37 downto 0) := "00000000000000000000000000000000000000"; 
signal async_out_data_signal    : STD_LOGIC_VECTOR(37 downto 0) := "00000000000000000000000000000000000000"; 
signal async_in_clk_signal      : STD_LOGIC;
signal async_out_clk_signal     : STD_LOGIC;
signal async_in_full_signal     : STD_LOGIC;
signal async_out_empty_signal   : STD_LOGIC;
signal async_in_wr_en_signal    : STD_LOGIC;
signal async_out_rd_en_signal   : STD_LOGIC;
signal async_out_valid_signal   : STD_LOGIC;

--Interface to TosNet signals
signal clock_50M_signal         : STD_LOGIC;
signal enable_tosnet_signal     : STD_LOGIC;
signal set_nodeid1_signal       : STD_LOGIC;
signal set_nodeid2_signal       : STD_LOGIC;
signal set_nodeid3_signal       : STD_LOGIC;
signal set_nodeid_signal        : STD_LOGIC_VECTOR(3 downto 0);
signal sensor_data_tos_in_signal   : STD_LOGIC_VECTOR ( 31 downto 0 );
signal sensor_data_tos_out_signal  : STD_LOGIC_VECTOR ( 31 downto 0 );

signal pwm_flag_tos, pwm_flag, motor_data_flag_resp_signal : STD_LOGIC := '0';

--Interface to memory interfaces
signal tosnet_to_motor          : STD_LOGIC_VECTOR(31 downto 0);
signal motor_data_signal, motor_data_tos_signal   : STD_LOGIC_VECTOR(31 downto 0);
signal data_req1, data_req2, data_ack1, data_ack2 : STD_LOGIC := '0';
signal data_req_tos1, data_req_tos2, data_ack_tos1, data_ack_tos2 : STD_LOGIC := '0';
signal data_req_dist1, data_req_dist2, data_req_dist3, data_ack_dist1, data_ack_dist2, data_ack_dist3 : STD_LOGIC := '0';
signal test_bit_signal          : STD_LOGIC := '0';
signal test_bit_tos_signal : STD_LOGIC := '0';
signal memory_synch_signal : STD_LOGIC := '0';          

--Signals for PWM
signal pwm1_signal,pwm2_signal,pwm3_signal,pwm4_signal,pwm5_signal,pwm6_signal,pwm7_signal,pwm8_signal,pwm9_signal,pwm10_signal,pwm11_signal,pwm12_signal,pwm13_signal,pwm14_signal,pwm15_signal,pwm16_signal : STD_LOGIC;

--attribute mark_debug : string;
--attribute mark_debug of motor_data_signal : signal is "true";
--attribute mark_debug of tosnet_to_motor : signal is "true";
--attribute mark_debug of data_reg_data_in_signal : signal is "true";


component system_wrapper is
  port (
      BRAM_PORTB_0_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_0_clk : in STD_LOGIC;
      BRAM_PORTB_0_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_0_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_0_en : in STD_LOGIC;
      BRAM_PORTB_0_rst : in STD_LOGIC;
      BRAM_PORTB_0_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
      BRAM_PORTB_1_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_1_clk : in STD_LOGIC;
      BRAM_PORTB_1_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_1_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
      BRAM_PORTB_1_en : in STD_LOGIC;
      BRAM_PORTB_1_rst : in STD_LOGIC;
      BRAM_PORTB_1_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
      DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
      DDR_cas_n : inout STD_LOGIC;
      DDR_ck_n : inout STD_LOGIC;
      DDR_ck_p : inout STD_LOGIC;
      DDR_cke : inout STD_LOGIC;
      DDR_cs_n : inout STD_LOGIC;
      DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
      DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_odt : inout STD_LOGIC;
      DDR_ras_n : inout STD_LOGIC;
      DDR_reset_n : inout STD_LOGIC;
      DDR_we_n : inout STD_LOGIC;
      FIXED_IO_ddr_vrn : inout STD_LOGIC;
      FIXED_IO_ddr_vrp : inout STD_LOGIC;
      FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
      FIXED_IO_ps_clk : inout STD_LOGIC;
      FIXED_IO_ps_porb : inout STD_LOGIC;
      FIXED_IO_ps_srstb : inout STD_LOGIC;
      IRQ : in STD_LOGIC
  );
end component;

component main_mem_v7 is
    Port ( 
        clock           : in STD_LOGIC;
        tosnet_enabled  : in STD_LOGIC;
        master_node     : in STD_LOGIC;
        BRAM_PORTB_0_addr : out STD_LOGIC_VECTOR ( 31 downto 0 );               
        BRAM_PORTB_0_we   : out STD_LOGIC_VECTOR ( 3 downto 0 );
        BRAM_PORTB_0_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
        BRAM_PORTB_1_addr : out STD_LOGIC_VECTOR ( 31 downto 0 );        
        BRAM_PORTB_1_din  : out STD_LOGIC_VECTOR ( 31 downto 0 );       
        BRAM_PORTB_1_we   : out STD_LOGIC_VECTOR ( 3 downto 0 );
        sensor_data     : in STD_LOGIC_VECTOR ( 31 downto 0 );
        sensor_data_tos : in STD_LOGIC_VECTOR ( 31 downto 0 );
        motor_data_pwm : out STD_LOGIC_VECTOR ( 31 downto 0 );
        data_req_out    : out STD_LOGIC;                        --local handshaking signals
        data_req_in     : in STD_LOGIC;
        data_ack_in     : in STD_LOGIC;
        data_ack_out    : out STD_LOGIC;
        data_req_tos_out    : out STD_LOGIC;                    --distributed memory handshaking signals
        data_req_tos_in     : in STD_LOGIC;
        data_ack_tos_in     : in STD_LOGIC;
        data_ack_tos_out    : out STD_LOGIC;
        synch_interrupt     : in STD_LOGIC;
        interrupt_flag  : out STD_LOGIC
     );
end component;

component distributed_memory_v9 is
    Port ( 
        clock               : in STD_LOGIC;
        tosnet_enabled      : in STD_LOGIC;
        tosnet_synch        : in STD_LOGIC;
        master_check        : in STD_LOGIC;
        node_sensor_data    : in STD_LOGIC_VECTOR (31 downto 0);    --direct input from sensor
        node_sensor_out     : out STD_LOGIC_VECTOR (31 downto 0); 
        motor_data_in       : in STD_LOGIC_VECTOR (31 downto 0);  --motor_array;        
        motor_data_tos_out      : out STD_LOGIC_VECTOR (31 downto 0);   --data to motors, calculations from ARM
        data_network_rcommit    : out STD_LOGIC;
        data_network_wcommit    : out STD_LOGIC;
        data_tosnet_in          : in STD_LOGIC_VECTOR(31 downto 0);
        data_tosnet_out         : out STD_LOGIC_VECTOR(31 downto 0);
        network_we              : out STD_LOGIC_VECTOR(0 downto 0);
        network_addr            : out STD_LOGIC_VECTOR(9 downto 0);
        network_clk             : out STD_LOGIC;
        data_req_s_out          : out STD_LOGIC;                        --handshaking signals to pass sensor data
        data_req_s_in           : in STD_LOGIC;
        data_ack_s_in           : in STD_LOGIC;
        data_ack_s_out          : out STD_LOGIC;
        data_req_m_out          : out STD_LOGIC;                        --handshaking signals to pass motor data
        data_req_m_in           : in STD_LOGIC;
        data_ack_m_in           : in STD_LOGIC;
        data_ack_m_out          : out STD_LOGIC;
        node_id                 : in STD_LOGIC_VECTOR(3 downto 0);
        synch_interrupt         : out STD_LOGIC := '0';                     
        system_response_out     : out STD_LOGIC := '0';                     --TESTING
        system_response_out_tos  : out STD_LOGIC := '0'                     --TESTING
     );
end component;

--component motor_data_v3 is
--    Port (
--        clock                : in STD_LOGIC;
--        master_check         : in STD_LOGIC; 
--        motor_data_local     : in STD_LOGIC_VECTOR (31 downto 0);
--        data_req_main        : in STD_LOGIC;
--        data_req_dist        : out STD_LOGIC;
--        data_ack_main        : out STD_LOGIC;
--        data_ack_dist        : in STD_LOGIC;
--        motor_data_out       : out motor_array
--    );
--end component;

component pc_pwm_v10 is
    Port ( 
           clk_50MHz        : in STD_LOGIC;
           tosnet_enabled   : in STD_LOGIC;
           tosnet_master    : in STD_LOGIC;
           data_req_loc_pwm     : in STD_LOGIC;
           data_req_tos_pwm     : in STD_LOGIC;
           data_ack_loc_pwm     : out STD_LOGIC;
           data_ack_tos_pwm     : out STD_LOGIC;
           motor_data_local : in STD_LOGIC_VECTOR (31 downto 0);
           motor_data_tosnet : in STD_LOGIC_VECTOR (31 downto 0);
           pulse_out      : out STD_LOGIC_VECTOR (15 downto 0)
           );
end component;

component xadc is
    Port ( 
        clk_50M : in STD_LOGIC;
        adc_vn14 : in STD_LOGIC;
        adc_vp14 : in STD_LOGIC;
        adc_vn7 : in STD_LOGIC;
        adc_vp7 : in STD_LOGIC;
        adc_vn15 : in STD_LOGIC;
        adc_vp15 : in STD_LOGIC;
        adc_vn6 : in STD_LOGIC;
        adc_vp6 : in STD_LOGIC;
        adc_out  : out STD_LOGIC_VECTOR(31 downto 0);
        data_req_loc_adc : out STD_LOGIC := '0';
        data_req_tos_adc : out STD_LOGIC := '0';
        data_ack_loc_adc : in STD_LOGIC := '0';
        data_ack_tos_adc : in STD_LOGIC := '0';
        tosnet_enabled : in STD_LOGIC;
        is_master      : in STD_LOGIC;
        local_node     : in STD_LOGIC_VECTOR (3 downto 0) := "0000";
        adc_out_tos    : out STD_LOGIC_VECTOR(31 downto 0)
    );
end component;

component tosnet is
    Generic (	
            disable_slave			: STD_LOGIC := '0';						
			disable_master			: STD_LOGIC := '0';					
			disable_async			: STD_LOGIC := '0'
	);					
    Port (	
            sig_in				    : in	STD_LOGIC;						
			sig_out					: out	STD_LOGIC;						
			clk_50M					: in	STD_LOGIC;						
			reset					: in	STD_LOGIC;						
			sync_strobe				: out	STD_LOGIC;						
			online					: out	STD_LOGIC;						
			is_master				: out	STD_LOGIC;						
			packet_error			: out	STD_LOGIC;						
			system_halt				: out	STD_LOGIC;						
			node_id					: in	STD_LOGIC_VECTOR(3 downto 0);	
			reg_enable				: in	STD_LOGIC_VECTOR(7 downto 0);	
			watchdog_threshold		: in	STD_LOGIC_VECTOR(17 downto 0);	
			max_skipped_writes		: in	STD_LOGIC_VECTOR(15 downto 0);	
			max_skipped_reads		: in	STD_LOGIC_VECTOR(15 downto 0);	
			data_reg_addr			: in	STD_LOGIC_VECTOR(9 downto 0);	
			data_reg_data_in		: in	STD_LOGIC_VECTOR(31 downto 0);	
			data_reg_data_out		: out	STD_LOGIC_VECTOR(31 downto 0);	
			data_reg_clk			: in	STD_LOGIC;						
			data_reg_we				: in	STD_LOGIC_VECTOR(0 downto 0);	
			commit_write 			: in	STD_LOGIC;						
			commit_read				: in	STD_LOGIC;						
			reset_counter			: out	STD_LOGIC_VECTOR(31 downto 0);	
			packet_counter			: out	STD_LOGIC_VECTOR(31 downto 0);	
			error_counter			: out	STD_LOGIC_VECTOR(31 downto 0);
			async_in_data			: in	STD_LOGIC_VECTOR(37 downto 0);	
			async_out_data			: out	STD_LOGIC_VECTOR(37 downto 0);	
			async_in_clk			: in	STD_LOGIC;						
			async_out_clk			: in	STD_LOGIC;						
			async_in_full			: out	STD_LOGIC;						
			async_out_empty			: out	STD_LOGIC;						
			async_in_wr_en			: in	STD_LOGIC;						
			async_out_rd_en			: in	STD_LOGIC;						
			async_out_valid			: out	STD_LOGIC 
		);                      
end component;

component tosnet_clock_wrapper is
  port (
    clk_out1_0 : out STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    sys_clock : in STD_LOGIC
  );
end component;

component debounce is
    Port ( 
        clock : in STD_LOGIC;
        switch_in : in STD_LOGIC;
        switch_out : out STD_LOGIC
    );
end component;

component set_tosnet_id is
    Port ( id1 : in STD_LOGIC;
           id2 : in STD_LOGIC;
           id3 : in STD_LOGIC;
           set_node_id : out STD_LOGIC_VECTOR (3 downto 0)
          );
end component;

begin

led_indicator(0) <= enable_tosnet_signal;
led_indicator(1) <= set_nodeid1_signal;
led_indicator(2) <= set_nodeid2_signal;
led_indicator(3) <= set_nodeid3_signal;

motor_out(0) <= pwm1_signal;
motor_out(1) <= pwm2_signal;
motor_out(2) <= pwm3_signal;
motor_out(3) <= pwm4_signal;
motor_out(4) <= pwm5_signal;
motor_out(5) <= pwm6_signal;
motor_out(6) <= pwm7_signal;
motor_out(7) <= pwm8_signal;
motor_out(8) <= pwm9_signal;
motor_out(9) <= pwm10_signal;
motor_out(10) <= pwm11_signal;
motor_out(11) <= pwm12_signal;
motor_out(12) <= pwm13_signal;
motor_out(13) <= pwm14_signal;
motor_out(14) <= pwm15_signal;
motor_out(15) <= pwm16_signal;

system_test_bit <= test_bit_signal;
system_test_bit_tos <= test_bit_tos_signal;
--system_test_bit <= sync_strobe_signal;          --TESTING, confirm cycling frequency of TosNet, output on Master node
--system_test_bit_tos <= sync_strobe_signal;

wrapper_instance: system_wrapper
    port map(
        BRAM_PORTB_0_addr => BRAM_PORTB_0_addr_signal,
        BRAM_PORTB_0_clk => clock_50M_signal,               
        BRAM_PORTB_0_din => BRAM_PORTB_0_din_signal, 
        BRAM_PORTB_0_dout => BRAM_PORTB_0_dout_signal,
        BRAM_PORTB_0_en => '1',
        BRAM_PORTB_0_rst => '0',
        BRAM_PORTB_0_we => BRAM_PORTB_0_we_signal,
        BRAM_PORTB_1_addr => BRAM_PORTB_1_addr_signal,
        BRAM_PORTB_1_clk => clock_50M_signal,
        BRAM_PORTB_1_din => BRAM_PORTB_1_din_signal,
        BRAM_PORTB_1_dout => BRAM_PORTB_1_dout_signal,
        BRAM_PORTB_1_en => '1',
        BRAM_PORTB_1_rst => '0',
        BRAM_PORTB_1_we => BRAM_PORTB_1_we_signal,
        IRQ => interrupt_flag_signal 
    );

main_mem_instance: main_mem_v7   
    port map( 
            clock           => clock_50M_signal,
            tosnet_enabled  => enable_tosnet_signal,       --(input) high if TosNet enabled, determines which signals are read / written
            master_node     => is_master_signal,           --(input) high if master node of TosNet, determines which state machine enabled
            BRAM_PORTB_0_addr => BRAM_PORTB_0_addr_signal, --(output) address in BRAM to read from (motor data)
            BRAM_PORTB_0_we   => BRAM_PORTB_0_we_signal,   --(output) write enable signal
            BRAM_PORTB_0_dout => BRAM_PORTB_0_dout_signal, --(input) data from ARM, will be sent to TosNet or motors (determined by TosNet enabled signal)
            BRAM_PORTB_1_addr => BRAM_PORTB_1_addr_signal, --(output) address in BRAM to write to (sensor data)
            BRAM_PORTB_1_din  => BRAM_PORTB_1_din_signal,  --(output) data to BRAM, sensor data to be read by ARM received from local sensors or TosNet slaves 
            BRAM_PORTB_1_we   => BRAM_PORTB_1_we_signal,   --(output) write enable signal        
            sensor_data     => sensor_data_signal,         --(input) data from ADC sensor
            sensor_data_tos => sensor_data_tos_out_signal, --(input) data from slave node sensors
            motor_data_pwm  => motor_data_signal,          --(output) sends data to motor component or local motors when single node
            data_req_out    => data_req1,                  --(output) sends request to pwm component to initiate 4-phase handshake
            data_req_in     => data_req2,                  --(input) receives request from adc component to initiate 4-phase handshake
            data_ack_in     => data_ack1,                  --(input) receives acknowledgement from receiver (motor)
            data_ack_out    => data_ack2,                  --(output) sends acknowledgement to sender (sensor)
            data_req_tos_out    => data_req_tos1,          --(output) sends request to motor data to initiate 4-phase handshake
            data_req_tos_in     => data_req_tos2,          --(input) receives request from distributed memory to initiate 4-phase handshake
            data_ack_tos_in     => data_ack_tos1,          --(input) receives acknowledgement from motor data
            data_ack_tos_out    => data_ack_tos2,          --(output) sends acknowledgement to distributed memory
            synch_interrupt     => memory_synch_signal,    --(input) triggers PL-PS interrupt from master through main to ARM
            interrupt_flag  => interrupt_flag_signal        --(output) triggers PL-PS interrupt, can be seen from the ARM
         ); 

distributed_mem_instance: distributed_memory_v9
     port map( 
        clock                => clock_50M_signal,
        tosnet_enabled       => enable_tosnet_signal,
        tosnet_synch         => sync_strobe_signal,                 --(input) synch state machine with TosNet cycle          
        master_check         => is_master_signal,
        node_sensor_data     => sensor_data_tos_in_signal,          --(input) direct input from sensor when slave
        node_sensor_out      => sensor_data_tos_out_signal,         --(output) master to main memeory, slave node sensor data
        motor_data_in        => motor_data_signal, --motor_data_array_signal,            --(input) motor data per node, read by master
        motor_data_tos_out   => tosnet_to_motor,
        data_network_rcommit => commit_read_signal,
        data_network_wcommit => commit_write_signal,
        data_tosnet_in       => data_reg_data_out_signal,           --(input) data from TosNet, master receives sensor data, slave receives motor data
        data_tosnet_out      => data_reg_data_in_signal,            --(output) data to TosNet, master sends motor data, slave sends sensor data
        network_we           => data_reg_we_signal,                 
        network_addr         => data_reg_addr_signal,
        network_clk          => data_reg_clk_signal,
        data_req_s_out       => data_req_tos2,
        data_req_s_in        => data_req_dist1,
        data_ack_s_in        => data_ack_tos2,
        data_ack_s_out       => data_ack_dist1,
        data_req_m_out       => data_req_dist2,
        data_req_m_in        => data_req_tos1, --data_req_dist3,                           
        data_ack_m_in        => data_ack_dist2,
        data_ack_m_out       => data_ack_tos1, --data_ack_dist3,                   
        node_id              => set_nodeid_signal,
        synch_interrupt      => memory_synch_signal,                
        system_response_out => test_bit_signal,                     --TESTING
        system_response_out_tos => test_bit_tos_signal              --TESTING
     );    

--motor_data_instance: motor_data_v3
--    port map(
--        clock               => clock_50M_signal,
--        master_check        => is_master_signal,
--        motor_data_local    => motor_data_signal,
--        data_req_main       => data_req_tos1,
--        data_req_dist       => data_req_dist3,
--        data_ack_main       => data_ack_tos1,
--        data_ack_dist       => data_ack_dist3,
--        motor_data_out      => motor_data_array_signal
--        );

pwm_instance: pc_pwm_v10
    port map( 
           clk_50MHz        => clock_50M_signal,              
           tosnet_enabled   => enable_tosnet_signal,
           tosnet_master    => is_master_signal,
           data_req_loc_pwm => data_req1,
           data_req_tos_pwm => data_req_dist2,
           data_ack_loc_pwm => data_ack1,
           data_ack_tos_pwm => data_ack_dist2,                        
           motor_data_local => motor_data_signal,                       --Local data to motor, use when single node
           motor_data_tosnet => tosnet_to_motor,                        --TosNet data to motor, use when node is slave
           pulse_out(0)     => pwm1_signal,
           pulse_out(1)     => pwm2_signal,
           pulse_out(2)     => pwm3_signal,
           pulse_out(3)     => pwm4_signal,
           pulse_out(4)     => pwm5_signal,
           pulse_out(5)     => pwm6_signal,
           pulse_out(6)     => pwm7_signal,
           pulse_out(7)     => pwm8_signal,
           pulse_out(8)     => pwm9_signal,
           pulse_out(9)     => pwm10_signal,
           pulse_out(10)    => pwm11_signal,
           pulse_out(11)    => pwm12_signal,
           pulse_out(12)    => pwm13_signal,
           pulse_out(13)    => pwm14_signal,
           pulse_out(14)    => pwm15_signal,
           pulse_out(15)    => pwm16_signal
    );

xadc_instance: xadc
    port map(
        clk_50M => clock_50M_signal,
        adc_vn14   => Vaux_v_n14,
        adc_vp14   => Vaux_v_p14,
        adc_vn7    => Vaux_v_n7,
        adc_vp7    => Vaux_v_p7,
        adc_vn15   => Vaux_v_n15,
        adc_vp15   => Vaux_v_p15,
        adc_vn6    => Vaux_v_n6,
        adc_vp6    => Vaux_v_p6,
        adc_out    => sensor_data_signal,
        data_req_loc_adc => data_req2,
        data_req_tos_adc => data_req_dist1,
        data_ack_loc_adc => data_ack2,
        data_ack_tos_adc => data_ack_dist1,
        tosnet_enabled => enable_tosnet_signal,
        is_master      => is_master_signal,
        local_node     => set_nodeid_signal, 
        adc_out_tos    => sensor_data_tos_in_signal 
    );
  
tosnet_inst: tosnet    
    Generic map(	
            disable_slave    => '0',                       
            disable_master   => '0',                    
            disable_async    => '1'
    )                    
    Port map(    
            sig_in              => sig_in,                  --(input) signal from receiver         
            sig_out             => sig_out,                 --(output) signal to transmitter          
            clk_50M             => clock_50M_signal,                   
            reset               => '0',                        
            sync_strobe         => sync_strobe_signal,      --(output) signals end of a cycle                
            online              => online_signal,           --(output) signals network setup successful - NOT IN USE                 
            is_master           => is_master_signal,        --(output) signals node acting as master when high, lowest Node ID automatically assigned as master 
            packet_error        => packet_error_signal,     --(output) signals error in transmission packet when high - NOT IN USE                       
            system_halt         => system_halt_signal,      --(output) signals max skipped registers exceeded when high, functionality disabled b/c max skipped r/w disabled - NOT IN USE                     
            node_id             => set_nodeid_signal,       --(input) receive ID from switch settings        
            reg_enable          => "00001111",              --enable only registers used
            watchdog_threshold  => "111111111111111111",    
            max_skipped_writes  => "0000000000000000",    
            max_skipped_reads   => "0000000000000000",    
            data_reg_addr       => data_reg_addr_signal,        --(input) indicates which part of the in or out block to target, 
            data_reg_data_in    => data_reg_data_in_signal,     --(input) data written to memory block
            data_reg_data_out   => data_reg_data_out_signal,    --(output) data read from memory block 
            data_reg_clk        => data_reg_clk_signal,         --(input) set high to perform a read or write              
            data_reg_we         => data_reg_we_signal,          --(input) set high to write, low to read
            commit_write        => commit_write_signal,         --(input) set high 20ns after write cycle (out block)             
            commit_read         => commit_read_signal,          --(input) set high 20ns after read cycle (in block)            
            reset_counter       => reset_counter_signal,        --(output) tracks number of resets since startup
            packet_counter      => packet_counter_signal,       --(output) tracks number of packets transmitted since startup
            error_counter       => error_counter_signal,        --(output) tracks number of errors since startup
            async_in_data       => async_in_data_signal,        --(input) data bus for receiving data from asynchronous FIFO buffers 
            async_out_data      => async_out_data_signal,       --(output) data bus for sending data to asynchronous FIFO buffers 
            async_in_clk        => async_in_clk_signal,                       
            async_out_clk       => async_out_clk_signal,                        
            async_in_full       => async_in_full_signal,                        
            async_out_empty     => async_out_empty_signal,                        
            async_in_wr_en      => async_in_wr_en_signal,                        
            async_out_rd_en     => async_out_rd_en_signal,                        
            async_out_valid     => async_out_valid_signal 
        );

clock_instance: tosnet_clock_wrapper                                                                                                                                                                                
      Port map(            
            clk_out1_0 => clock_50M_signal,
            reset_rtl => '0',                                                                                                                                                                       
            sys_clock => clock_125M     --only place where 125M clock should be used, supplies the IP core which outputs 50MHz to all other components
        );                                                                                                                                                                        
                                                                                                        
debounce_tosnet_instance: debounce                                                                                                                                                                                          
Port map (                                                                                                                                                                  
         clock => clock_50M_signal, 
         switch_in => sw_setting(0),
         switch_out => enable_tosnet_signal
    );

debounce_nodeid1_instance: debounce
Port map ( 
         clock => clock_50M_signal, 
         switch_in => sw_setting(1),
         switch_out => set_nodeid1_signal
    );

debounce_nodeid2_instance: debounce
Port map ( 
         clock => clock_50M_signal, 
         switch_in => sw_setting(2),
         switch_out => set_nodeid2_signal
    );
    
debounce_nodeid3_instance: debounce
Port map ( 
         clock => clock_50M_signal, 
         switch_in => sw_setting(3),
         switch_out => set_nodeid3_signal
    );

set_tosnet_id_instance: set_tosnet_id
Port map ( id1 => set_nodeid1_signal,
           id2 => set_nodeid2_signal,
           id3 => set_nodeid3_signal,
           set_node_id => set_nodeid_signal
          );

end Behavioral;