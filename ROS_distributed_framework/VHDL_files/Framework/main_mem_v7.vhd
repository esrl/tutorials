library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity main_mem_v7 is
    Port ( 
        clock           : in STD_LOGIC;
        tosnet_enabled  : in STD_LOGIC;
        master_node     : in STD_LOGIC;
        BRAM_PORTB_0_addr : out STD_LOGIC_VECTOR (31 downto 0);              
        BRAM_PORTB_0_we   : out STD_LOGIC_VECTOR (3 downto 0);
        BRAM_PORTB_0_dout : in STD_LOGIC_VECTOR (31 downto 0);               --data from ARM, send to TosNet or motors
        BRAM_PORTB_1_addr : out STD_LOGIC_VECTOR (31 downto 0);        
        BRAM_PORTB_1_din  : out STD_LOGIC_VECTOR (31 downto 0);              --data to ARM from sensors      
        BRAM_PORTB_1_we   : out STD_LOGIC_VECTOR (3 downto 0);
        sensor_data     : in STD_LOGIC_VECTOR (31 downto 0);                 --direct input from sensor  
        sensor_data_tos : in STD_LOGIC_VECTOR (31 downto 0);   
        motor_data_pwm  : out STD_LOGIC_VECTOR (31 downto 0) := x"00000000"; --motor data for slave and PWM component
        data_req_out    : out STD_LOGIC;                                     --local handshaking signals
        data_req_in     : in STD_LOGIC;
        data_ack_in     : in STD_LOGIC;
        data_ack_out    : out STD_LOGIC;
        data_req_tos_out    : out STD_LOGIC;                                 --distributed memory handshaking signals
        data_req_tos_in     : in STD_LOGIC;
        data_ack_tos_in     : in STD_LOGIC;
        data_ack_tos_out    : out STD_LOGIC;
        synch_interrupt : in STD_LOGIC;
        interrupt_flag  : out STD_LOGIC
     );
end main_mem_v7;

architecture Behavioral of main_mem_v7 is

--signals for reading and writing to main memory
signal data_to_memory        : STD_LOGIC_VECTOR(31 downto 0);   
signal wmemory_address, rmemory_address : STD_LOGIC_VECTOR (31 downto 0);
signal wwrite_enable, rwrite_enable     : STD_LOGIC_VECTOR (3 downto 0);
signal interrupt_trigger   : STD_LOGIC := '0';
signal interrupt_read, interrupt_write : STD_LOGIC := '0';
signal motor_data_out        : STD_LOGIC_VECTOR(31 downto 0) := x"00000000";
signal motor_data_ready      : STD_LOGIC := '0';
signal sensor_data_tos_int   : STD_LOGIC_VECTOR (31 downto 0);
signal data_req_loc_flag, data_ack_loc_flag    : STD_LOGIC;
signal data_req_tos_flag, data_ack_tos_flag    : STD_LOGIC;

type wmemory_state_type is (wmemory_wait, wmemory_update_address, wmemory_write_data);
type rmemory_state_type is (rmemory_wait, rmemory_update_address, rmemory_read_data);
signal wmemory_state, wmemory_next_state : wmemory_state_type;
signal rmemory_state, rmemory_next_state  : rmemory_state_type;

--signals for prescaling main clock
signal prescale_clock : unsigned(31 downto 0) := x"00000000";
signal memory_clock   : std_logic;

begin
 
     memory_clock <= (prescale_clock(1));   
     
prescale: process(clock)
begin
   if rising_edge(clock) then
       prescale_clock <= prescale_clock + 1;
   end if;    
end process; 

NEXT_STATE_PROC: process (clock)    
begin
    if rising_edge(clock) then
     if (tosnet_enabled = '0' OR (tosnet_enabled = '1' AND master_node = '1')) then
         wmemory_state <= wmemory_next_state;
         rmemory_state <= rmemory_next_state;
     else wmemory_state <= wmemory_state;
         rmemory_state <= rmemory_state;
     end if;
    end if;
end process;

SYNC_PROC_READ: process (memory_clock)
     
 --variables for main memory 
 variable memory_current_address : unsigned(31 downto 0) := x"00000000";   --initialize address to first overall memory slot
 variable memory_increment       : unsigned(15 downto 0) := x"0000";         
 variable read_write             : STD_LOGIC := '0';                        
 variable data_ack_flag_int, data_ack_flag_tos_int    : STD_LOGIC := '0';
 variable data_req_flag_int, data_req_flag_tos_int    : STD_LOGIC := '0';
 
 begin
 if rising_edge(memory_clock) then
   if (tosnet_enabled = '0' OR (tosnet_enabled = '1' AND master_node = '1')) then        
   
   case(rmemory_state) is
   when rmemory_wait => rwrite_enable <= "0000";
                       if interrupt_trigger = '1' then interrupt_read <= '0'; end if;
                       rmemory_next_state <= rmemory_update_address;          
   when rmemory_update_address =>
       if memory_increment >= 127 then
       --if memory_increment >= 255 then                            --reset address and increment value when end of the memory block reached, 2048 memory addresses (1024 for FPGA; 1024 for ARM) - limit to 1024, max addresses usable by TosNet (max is 255, bc counter not incremented when jumping to next block)
           memory_current_address := x"00000000";
           memory_increment := x"0000";
           interrupt_read <= '1';
       else
           memory_current_address := memory_current_address + 4;         --increment to next immediate address
           memory_increment := memory_increment + 1;
       end if;
       
   rmemory_address <= std_logic_vector(memory_current_address);
   rmemory_next_state <= rmemory_read_data;
                                 
   when rmemory_read_data =>     
                          rwrite_enable <= "0000";
                          if tosnet_enabled = '0' then
                              data_req_loc_flag <= '1';
                              data_ack_flag_int := data_ack_in;
                              if data_ack_flag_int = '1' then              
                                 data_req_loc_flag <= '0';
                                 motor_data_out <= BRAM_PORTB_0_dout;   
                                 rmemory_next_state <= rmemory_wait;
                              end if;
                          elsif tosnet_enabled = '1' AND master_node = '1' then
                              data_req_tos_flag <= '1';
                              data_ack_flag_tos_int := data_ack_tos_in;
                              if data_ack_flag_tos_int = '1' then              
                                 data_req_tos_flag <= '0'; 
                                 motor_data_out <= BRAM_PORTB_0_dout;  
                                 rmemory_next_state <= rmemory_wait;
                              end if; 
                          end if;
   when others =>  rwrite_enable <= "0000";
                   interrupt_read <= '0';                         
   end case;

 end if;
end if; 
end process;

SYNC_PROC_WRITE: process (memory_clock)
     
 --variables for main memory 
 variable memory_current_address : unsigned(31 downto 0) := x"00001000";   --initialize address to first memory slot of second half of memory block
 variable memory_increment       : unsigned(15 downto 0) := x"0000";                               
 variable data_ack_flag_int, data_ack_flag_tos_int    : STD_LOGIC := '0';
 variable data_req_flag_int, data_req_flag_tos_int    : STD_LOGIC := '0';
 variable memory_assigned_address, memory_assigned_address_tos : unsigned(7 downto 0) := x"00";          
 
 begin
 if rising_edge(memory_clock) then
   if (tosnet_enabled = '0' OR (tosnet_enabled = '1' AND master_node = '1')) then        
   
   case(wmemory_state) is
   when wmemory_wait => wwrite_enable <= "0000";
                        if interrupt_trigger = '1' then interrupt_write <= '0'; end if;
                        wmemory_next_state <= wmemory_update_address;    
   
   when wmemory_update_address =>
       if memory_increment >= 31 then                            --reset address and increment value when end of the memory needed reached (max 60 sensors if all 15 slave nodes attached)
           memory_current_address := x"00001000";
           memory_increment := x"0000";
           interrupt_write <= '1';
       else
           memory_current_address := memory_current_address + 4;         --increment to next immediate address
           memory_increment := memory_increment + 1;
       end if;
       
   wmemory_next_state <= wmemory_write_data;
                        
   when wmemory_write_data => 
                            wwrite_enable <= "1111"; 
                            if (tosnet_enabled = '1' AND master_node = '1') then 
                                data_req_flag_tos_int := data_req_tos_in;           
                                if data_req_flag_tos_int = '1' AND data_ack_tos_flag = '0' then
                                    data_ack_tos_flag <= '1'; 
                                    data_ack_tos_out <= '1';   
                                    data_to_memory <= sensor_data_tos;
                                    memory_assigned_address_tos := ("00" & unsigned(sensor_data_tos(29 downto 24))) sll 2;     
                                    wmemory_address <= x"000010" & std_logic_vector(memory_assigned_address_tos);     
                                elsif data_req_flag_tos_int = '0' AND data_ack_tos_flag = '1' then
                                    data_ack_tos_flag <= '0'; 
                                    data_ack_tos_out <= '0';    
                                    wmemory_next_state <= wmemory_wait;
                                end if;    
                            elsif tosnet_enabled = '0' then
                                data_req_flag_int := data_req_in;           
                                if data_req_flag_int = '1' AND data_ack_loc_flag = '0' then
                                    data_ack_loc_flag <= '1'; 
                                    data_ack_out <= '1';   
                                    data_to_memory <= sensor_data;
                                     memory_assigned_address := ("00" & unsigned(sensor_data(29 downto 24))) sll 2;         
                                     wmemory_address <= x"000010" & std_logic_vector(memory_assigned_address);     
                                elsif data_req_flag_int = '0' AND data_ack_loc_flag = '1' then
                                    data_ack_loc_flag <= '0';  
                                    data_ack_out <= '0';   
                                    wmemory_next_state <= wmemory_wait;
                                end if;
                            end if;          
   
   when others =>  wwrite_enable <= "0000";
                   interrupt_write <= '0';                         
   end case;

 end if;
end if; 
end process;

INTERRUPT_CHECK: process (interrupt_write,interrupt_read,synch_interrupt)
variable dist_interrupt_status, trigger_status : STD_LOGIC := '0';
begin
    if tosnet_enabled = '0' then
        interrupt_trigger <= interrupt_read AND interrupt_write;
    elsif (tosnet_enabled = '1' AND master_node = '1') then
        if synch_interrupt = '1' then dist_interrupt_status :='1'; end if;
        interrupt_trigger <= dist_interrupt_status AND interrupt_read AND interrupt_write;
        trigger_status := dist_interrupt_status AND interrupt_read AND interrupt_write;
        if trigger_status = '1' then trigger_status := '0'; dist_interrupt_status := '0'; end if;
    end if;    

end process;

OUTPUT_DECODE: process (memory_clock)
begin
if rising_edge(memory_clock) then
   if tosnet_enabled = '0' then 
 --if (tosnet_enabled = '0' OR (tosnet_enabled = '1' AND master_node = '1')) then
     BRAM_PORTB_0_addr <= rmemory_address;      
     BRAM_PORTB_0_we   <= rwrite_enable;
     BRAM_PORTB_1_addr <= wmemory_address;      
     BRAM_PORTB_1_we   <= wwrite_enable;
     interrupt_flag    <= interrupt_trigger;
     BRAM_PORTB_1_din  <= data_to_memory;
     motor_data_pwm    <= motor_data_out;
     data_req_out      <=  data_req_loc_flag;
     --data_ack_out      <=  data_ack_loc_flag; 
  elsif tosnet_enabled = '1' AND master_node = '1' then   
     BRAM_PORTB_0_addr <= rmemory_address;      
     BRAM_PORTB_0_we   <= rwrite_enable;
     BRAM_PORTB_1_addr <= wmemory_address;      
     BRAM_PORTB_1_we   <= wwrite_enable;
     interrupt_flag    <= interrupt_trigger;
     BRAM_PORTB_1_din  <= data_to_memory;
     motor_data_pwm    <= motor_data_out;
     data_req_tos_out  <= data_req_tos_flag;
     --data_ack_tos_out  <= data_ack_tos_flag;
  end if;
end if;
end process;


end Behavioral;