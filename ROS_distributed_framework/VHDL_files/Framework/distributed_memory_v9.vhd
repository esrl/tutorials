library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package motors is
    
   type motor_data_array is array (0 to 15) of std_logic_vector(31 downto 0);
   type motor_array is array (0 to 14) of motor_data_array;

end motors;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.motors.ALL;

entity distributed_memory_v9 is
    Port ( 
        clock               : in STD_LOGIC;
        tosnet_enabled      : in STD_LOGIC;
        tosnet_synch        : in STD_LOGIC;
        master_check        : in STD_LOGIC := '0';
        node_sensor_data    : in STD_LOGIC_VECTOR (31 downto 0);    --direct input from sensor
        node_sensor_out     : out STD_LOGIC_VECTOR (31 downto 0);  
        motor_data_in       : in STD_LOGIC_VECTOR (31 downto 0);
        motor_data_tos_out      : out STD_LOGIC_VECTOR (31 downto 0);
        data_network_rcommit    : out STD_LOGIC;
        data_network_wcommit    : out STD_LOGIC;
        data_tosnet_in          : in STD_LOGIC_VECTOR(31 downto 0);
        data_tosnet_out         : out STD_LOGIC_VECTOR(31 downto 0);
        network_we              : out STD_LOGIC_VECTOR(0 downto 0);
        network_addr            : out STD_LOGIC_VECTOR(9 downto 0);
        network_clk             : out STD_LOGIC;
        data_req_s_out          : out STD_LOGIC;                        --handshaking signals to pass sensor data
        data_req_s_in           : in STD_LOGIC;
        data_ack_s_in           : in STD_LOGIC;
        data_ack_s_out          : out STD_LOGIC;
        data_req_m_out          : out STD_LOGIC;                        --handshaking signals to pass motor data
        data_req_m_in           : in STD_LOGIC;
        data_ack_m_in           : in STD_LOGIC;
        data_ack_m_out          : out STD_LOGIC;
        node_id                 : in STD_LOGIC_VECTOR(3 downto 0);
        synch_interrupt         : out STD_LOGIC := '0';                    
        system_response_out  : out STD_LOGIC := '0';                        --TESTING
        system_response_out_tos  : out STD_LOGIC := '0'                     --TESTING
     );
end distributed_memory_v9;

architecture Behavioral of distributed_memory_v9 is

--signals for reading and writing to TosNet
signal data_to_memory_master        : STD_LOGIC_VECTOR(31 downto 0);                          --signal for sending data to main BRAM
signal data_to_tosnet_master, data_to_tosnet_slave        : STD_LOGIC_VECTOR(31 downto 0) := x"00000000";    --signals for sending data to TosNet 
signal write_enable_master, write_enable_slave            : STD_LOGIC_VECTOR(0 downto 0) := "0";
signal set_network_clk_master, set_network_clk_slave      : STD_LOGIC := '0';
signal rcommit_mas, wcommit_mas, rcommit_sl, wcommit_sl   : STD_LOGIC := '0';
signal update_addr_master, update_addr_slave  		      : STD_LOGIC_VECTOR(9 downto 0);
signal read_write_slave_signal  : STD_LOGIC := '0';                                           --start in read mode
signal motor_data_signal   : std_logic_vector(31 downto 0);
signal data_req_s_flag, data_ack_s_flag    : STD_LOGIC;
signal data_req_m_flag, data_ack_m_flag    : STD_LOGIC;
signal data_req_int_flag, data_ack_int_flag : STD_LOGIC;

--signals for state machines
type master_state_type is (master_wait, master_update_address, master_write_data, master_read_data);
type slave_state_type is (slave_wait, slave_update_address, slave_write_data, slave_read_data);
type motor_state_type is (motor_write_data, motor_read_data);
signal master_state, master_next_state  : master_state_type;
signal slave_state, slave_next_state    : slave_state_type;
signal motor_state, motor_next_state    : motor_state_type;

signal motor_data_int, motor_data_int_master : motor_array;

--signals for prescaling main clock
signal prescale_clock : unsigned(31 downto 0) := x"00000000";
signal memory_clock, slave_clock   : std_logic;

signal system_response, system_response_tos : STD_LOGIC := '0';   --TESTING
signal adc_data_read_tos_internal : STD_LOGIC := '0';             
signal msb_tos : STD_LOGIC_VECTOR(1 downto 0) := "00";            --TESTING
signal strobe_signal : unsigned (31 downto 0) := x"00000000"; 
signal master_interrupt : STD_LOGIC := '0';          

begin
 
     memory_clock <= (prescale_clock(1));   
     slave_clock <= (prescale_clock(1));            

prescale: process(clock)
 begin
   if rising_edge(clock) then
       prescale_clock <= prescale_clock + 1;
   end if;    
 end process;

NEXT_STATE_PROC: process (clock)    
begin

    if rising_edge(clock) then
        if tosnet_enabled = '1' AND master_check = '1' then
            master_state <= master_next_state;
            motor_state <= motor_next_state;
        elsif tosnet_enabled = '1' AND master_check = '0' then
            slave_state <= slave_next_state;
        else 
            master_state <= master_state;
            slave_state <= slave_state;
        end if;
    end if;
end process;

SYNC_PROC_MASTER: process (memory_clock)

--variables to track TosNet address
variable increment_master           : unsigned(1 downto 0) := "00";           --initialize address incrementer
variable reg_increment_master   : unsigned(2 downto 0) := "000";              --initialize register incrementer
variable node_increment         : unsigned(3 downto 0) := "0001";             --initialize node incrementer
variable inout_block_master     : STD_LOGIC_VECTOR(0 downto 0) := "0";        --initialize in with 'read' address
variable local_motor_id_master : unsigned(7 downto 0) := x"01";
variable local_motor_id_addressing : unsigned(7 downto 0) := x"00";
variable rw_flag_master : STD_LOGIC := '0';
variable read_write_master : STD_LOGIC := '0';
variable data_req_m_int, data_ack_s_int, data_req_motor_flag : STD_LOGIC := '0';
variable wait_trigger : STD_LOGIC := '0';
variable motor_msb_tos    : STD_LOGIC_VECTOR (0 downto 0) := "0";               --TESTING
variable sm_trigger_master : STD_LOGIC  := '0';                                 
variable strobe_synch_master, strobe_synch_old_master : unsigned (31 downto 0) := x"00000000";         

begin
if rising_edge(memory_clock) then
--**************************MASTER*********************************
    if (tosnet_enabled = '1' AND master_check = '1') then                    --wait until TosNet enabled, SM for master node      
 
       case(master_state) is
       when master_wait =>
        rcommit_mas <= '0';
        set_network_clk_master <= '0';  
                      
        if read_write_master = '0' then 
            write_enable_master <= "0";
            master_next_state <= master_update_address;                   
        elsif read_write_master = '1' then
            write_enable_master <= "1";                        
            master_next_state <= master_write_data; 
        end if;                              
       
       when master_update_address =>   
        set_network_clk_master <= '1';
        
        if node_increment <= 7 then                                         --UPDATED FROM 15
            if reg_increment_master <= 3 then                               --UPDATED FROM 7                                                                          
                if (increment_master = 3 AND reg_increment_master = 3 AND node_increment = 7) then       
                    increment_master := "00";
                    reg_increment_master := "000";
                    node_increment := "0001";
                    local_motor_id_master := x"01";                                                      --start writing at motor 1 for each node
                    if read_write_master = '0' then 
                        rcommit_mas <= '1';                                                              --commit read
                        local_motor_id_master := x"01";                                                  --start writing at motor 1 for each node
                        inout_block_master := "1";                                                       --switch to start writing to 'in block' (data to slaves)
                        rw_flag_master := '1';   
                        read_write_master := '1';                           
                    elsif read_write_master = '1' then 
                        wcommit_mas <= '1';                                                              --commit write 
                        inout_block_master := "0";
                        read_write_master := '0';
                        sm_trigger_master := '1';                                                              
                        master_interrupt <= '1';                                                                                       
                    end if;        
                elsif (increment_master = 3 AND reg_increment_master = 3 AND node_increment < 7) then
                    reg_increment_master := "000";
                    increment_master := "00"; 
                    node_increment := node_increment + 1;                                                                       --increment to next node once all addresses within one node are written
                    local_motor_id_master := x"01";                                                                                 --start writing at motor 1 for each node                                                      
                elsif (increment_master = 3 AND reg_increment_master < 3 AND node_increment = 7) then    
                    increment_master := "00";
                    reg_increment_master := reg_increment_master + 1;                                                           --increment to next register
                    local_motor_id_master := local_motor_id_master + 1;                                                         --increment motor ID when address incremented
                elsif (increment_master = 3 AND reg_increment_master < 3 AND node_increment < 7) then    
                    increment_master := "00";
                    reg_increment_master := reg_increment_master + 1;                                                           --increment to next register
                    local_motor_id_master := local_motor_id_master + 1;                                                         --increment motor ID when address incremented
                elsif increment_master < 3 then        
                    increment_master := increment_master + 1;                                                                   --increment to next address within the register
                    local_motor_id_master := local_motor_id_master + 1;                                                         --increment motor ID when address incremented
                end if;    
            end if;
        end if;

        if read_write_master = '0' then 
            write_enable_master <= "0";         
            update_addr_master <= std_logic_vector(node_increment) & std_logic_vector(reg_increment_master) & inout_block_master & std_logic_vector(increment_master);    
            master_next_state <= master_read_data; 
        end if;
        if read_write_master = '1' then             
            master_next_state <= master_wait; 
        end if;
        
       when master_write_data => if rw_flag_master = '1' then update_addr_master <= std_logic_vector(node_increment) & std_logic_vector(reg_increment_master) & inout_block_master & std_logic_vector(increment_master); rw_flag_master := '0'; end if;             
                                 --if local_motor_id_master <= 16 then
                                 data_req_motor_flag := data_req_int_flag;                        
                                 if data_req_motor_flag = '1' AND data_ack_int_flag = '0' then
                                     data_ack_int_flag <= '1';                                                                     --only write one value per motor per write cycle 
                                     local_motor_id_addressing := local_motor_id_master - 1;
                                     data_to_tosnet_master <= motor_data_int_master(to_integer(node_increment)-1)(to_integer(local_motor_id_master)-1);     --write motor data to TosNet
                                     --motor_msb_tos := motor_data_int_master(to_integer(node_increment)-1)(to_integer(local_motor_id_master)-1)(31 downto 31);    --TESTING
                                    -- if motor_msb_tos = "1" then system_response <= system_response xor '1'; end if;                                      --TESTING
                                     update_addr_master <= std_logic_vector(node_increment) & "0" & std_logic_vector(local_motor_id_addressing(3 downto 2)) & inout_block_master & std_logic_vector(local_motor_id_addressing(1 downto 0));            
                                 --end if;
                                 elsif data_req_motor_flag = '0' AND data_ack_int_flag = '1' then
                                    data_ack_int_flag <= '0';
                                    master_next_state <= master_update_address;
                                 end if;   
       when master_read_data => wcommit_mas <= '0';
                                master_interrupt <= '0';                                                                
                                set_network_clk_master <= '0';                                                         
                                if sm_trigger_master = '0' then
                                    data_req_s_flag <= '1';
                                    data_ack_s_int := data_ack_s_in;
                                    if data_ack_s_int = '1' then              
                                       data_req_s_flag <= '0';
                                       data_to_memory_master <= data_tosnet_in;     --read TosNet data, write it to main memory (slave sensor data, out blocks)                            
                                       master_next_state <= master_wait;   
                                    end if;
                                elsif sm_trigger_master = '1' then
                                     strobe_synch_master := strobe_signal;
                                     if strobe_synch_master /= strobe_synch_old_master then
                                         strobe_synch_old_master := strobe_synch_master;
                                         sm_trigger_master := '0';
                                         master_next_state <= master_read_data;
                                      end if;
                                 end if;                      
        
       when others =>                                                        --set all signals low if TosNet disabled to ensure accidental data is not written
           write_enable_master <= "0";                                       
           set_network_clk_master <= '0';                                 
           rcommit_mas <= '0';
           wcommit_mas <= '0';
     end case;        
     end if;
end if;
end process;
   
SYNC_PROC_SLAVE: process (slave_clock)

--variables to track TosNet address
variable increment_slave           : unsigned(1 downto 0) := "00";           --initialize address incrementer
variable reg_increment_slave   : unsigned(2 downto 0) := "000";              --initialize register incrementer
variable local_sensor_id_slave : integer range 0 to 3 := 0;
variable inout_block_slave     : STD_LOGIC_VECTOR(0 downto 0) := "1";
variable rw_flag_slave : STD_LOGIC := '0';
variable read_write_slave : STD_LOGIC := '0';
variable data_req_s_int, data_ack_m_int : STD_LOGIC := '0';
variable slave_timeout_counter : integer range 0 to 10 := 0;
variable local_sensor_id_addressing : unsigned(7 downto 0) := x"00";
variable motor_msb_tos    : STD_LOGIC_VECTOR (0 downto 0) := "0";           --TESTING
variable msb_update_tos   : std_logic_vector(1 downto 0) := "00";           --TESTING
variable sm_trigger       : STD_LOGIC  := '0';                              
variable strobe_synch, strobe_synch_old : unsigned (31 downto 0) := x"00000000";                     
    
begin

if rising_edge(slave_clock) then
--**************************SLAVE*********************************
    if (tosnet_enabled = '1' AND master_check = '0') then                    --wait until TosNet enabled, SM for slave node      
 
       case(slave_state) is
        when slave_wait =>
        rcommit_sl <= '0';
        set_network_clk_slave <= '0';                                                         
        if read_write_slave = '0' then
            write_enable_slave <= "0";
            slave_next_state <= slave_update_address;                        
        elsif read_write_slave = '1' then
            write_enable_slave <= "1";                                                                           
            slave_next_state <= slave_write_data;
        end if;                                        
       
       when slave_update_address =>   
        set_network_clk_slave <= '1'; 
          
        if reg_increment_slave <= 3 then                                        --UPDATED FROM 7                                               
            if (increment_slave = 3 AND reg_increment_slave = 3) then       
                increment_slave := "00";
                reg_increment_slave := "000";
                if read_write_slave = '0' then 
                    rcommit_sl <= '1';                                                              --commit read
                    inout_block_slave := "0";                                                       --switch to start writing to 'out block' (local sensor data to master)
                    rw_flag_slave := '1';           
                    read_write_slave := '1';                                  
                elsif read_write_slave = '1' then 
                    wcommit_sl <= '1';
                    inout_block_slave := "1";                                                       --switch to start reading from 'in block' (data from master)
                    read_write_slave := '0'; 
                    sm_trigger := '1';                                                                                
                end if;      
            elsif (increment_slave = 3 AND reg_increment_slave < 3) then    
                increment_slave := "00";
                reg_increment_slave := reg_increment_slave + 1;                             --increment to next register
            elsif increment_slave < 3 then        
                increment_slave := increment_slave + 1;                                     --increment to next address within the register
            end if;    
        end if;

        if read_write_slave = '0' then
            update_addr_slave <= node_id & std_logic_vector(reg_increment_slave) & inout_block_slave & std_logic_vector(increment_slave);  --only read/write to blocks for local node  
            write_enable_slave <= "0";      
            slave_next_state <= slave_read_data;
        end if;
        if read_write_slave = '1' then     
            slave_next_state <= slave_wait; 
        end if;
        
       when slave_write_data =>      if rw_flag_slave = '1' then update_addr_slave <= node_id & std_logic_vector(reg_increment_slave) & inout_block_slave & std_logic_vector(increment_slave); rw_flag_slave := '0'; end if;                           
                                     data_req_s_int := data_req_s_in;                       --handshake with sensor input (ADC)   
                                     if data_req_s_int = '1' AND data_ack_s_flag = '0' then
                                         data_ack_s_flag <= '1';
                                         data_ack_s_out <= '1';
                                         msb_update_tos := msb_tos;
                                         data_to_tosnet_slave <= msb_update_tos & node_sensor_data(29 downto 0);
                                         if msb_update_tos = "01" then
                                             adc_data_read_tos_internal <= adc_data_read_tos_internal xor '1';
                                         end if;    
                                         local_sensor_id_addressing := unsigned(node_sensor_data(31 downto 24))-1;               
                                         update_addr_slave <= node_id & std_logic_vector(reg_increment_slave) & inout_block_slave & std_logic_vector(local_sensor_id_addressing(1 downto 0));      
                                     elsif data_req_s_int = '0' AND data_ack_s_flag = '1' then
                                         data_ack_s_flag <= '0';   
                                         data_ack_s_out <= '0';  
                                         slave_next_state <= slave_update_address; 
                                     end if;
                                 
       when slave_read_data =>   wcommit_sl <= '0'; 
                                 set_network_clk_slave <= '0';                                                             
                                 if sm_trigger = '0' then                   
                                     data_req_m_flag <= '1';
                                     data_ack_m_int := data_ack_m_in;
                                     if data_ack_m_int = '1' then              
                                        data_req_m_flag <= '0';
                                        motor_msb_tos := data_tosnet_in(31 downto 31);    
                                        if motor_msb_tos = "1" then system_response_tos <= system_response_tos xor '1'; end if;             --system speed test bit inverter
                                        motor_data_signal <= data_tosnet_in;                       
                                        slave_next_state <= slave_wait;     
                                     end if;
                                 elsif sm_trigger = '1' then
                                     strobe_synch := strobe_signal;
                                     if strobe_synch /= strobe_synch_old then
                                         strobe_synch_old := strobe_synch;
                                         sm_trigger := '0';
                                         slave_next_state <= slave_read_data;
                                      end if;
                                 end if; 
                                 
                                 
       when others =>
           write_enable_slave <= "0";             --set all signals low if TosNet disabled to ensure accidental data is not written
           set_network_clk_slave <= '0';                                 
           rcommit_sl <= '0';
           wcommit_sl <= '0';  
       end case;  
     end if;
end if;

end process;

motor_data_process: process(memory_clock)                            
variable motor_id   : std_logic_vector(7 downto 0)  := x"00";
variable motor_pos    : std_logic_vector(15 downto 0) := x"0000";
variable motor_type   : std_logic_vector(7 downto 0) := x"00";
variable data_req_main_int, data_ack_int    : STD_LOGIC := '0';
variable motor_msb_tos    : STD_LOGIC_VECTOR (0 downto 0) := "0"; 

begin
if rising_edge(memory_clock) then
    if (tosnet_enabled = '1' AND master_check = '1') then 
    
    case(motor_state) is
        when motor_read_data =>
            data_req_main_int := data_req_m_in;                        
            if data_req_main_int = '1' AND data_ack_m_flag = '0' then
                data_ack_m_flag <= '1';
                data_ack_m_out <= '1';    
                motor_type := motor_data_in(31 downto 24);
                motor_id   := motor_data_in(23 downto 16);
                motor_pos  := motor_data_in(15 downto 0);
                
                motor_msb_tos := motor_data_in(31 downto 31); 
                if motor_msb_tos = "1" then system_response <= system_response xor '1'; end if;
                                    
                if motor_id >= x"01" AND motor_id <= x"10" then motor_data_int(0)(to_integer(unsigned(motor_id)-1)) <= motor_type & std_logic_vector(unsigned(motor_id)) & motor_pos; end if;
                if motor_id >= x"11" AND motor_id <= x"20" then motor_data_int(1)(to_integer(unsigned(motor_id)-17)) <= motor_type & std_logic_vector(unsigned(motor_id) - 16) & motor_pos; end if;
                if motor_id >= x"21" AND motor_id <= x"30" then motor_data_int(2)(to_integer(unsigned(motor_id)-33)) <= motor_type & std_logic_vector(unsigned(motor_id) - 32) & motor_pos; end if;
                if motor_id >= x"31" AND motor_id <= x"40" then motor_data_int(3)(to_integer(unsigned(motor_id)-49)) <= motor_type & std_logic_vector(unsigned(motor_id) - 48) & motor_pos; end if;
                if motor_id >= x"41" AND motor_id <= x"50" then motor_data_int(4)(to_integer(unsigned(motor_id)-65)) <= motor_type & std_logic_vector(unsigned(motor_id) - 64) & motor_pos; end if;
                if motor_id >= x"51" AND motor_id <= x"60" then motor_data_int(5)(to_integer(unsigned(motor_id)-81)) <= motor_type & std_logic_vector(unsigned(motor_id) - 80) & motor_pos; end if;
                if motor_id >= x"61" AND motor_id <= x"70" then motor_data_int(6)(to_integer(unsigned(motor_id)-97)) <= motor_type & std_logic_vector(unsigned(motor_id) - 96) & motor_pos; end if;
                if motor_id >= x"71" AND motor_id <= x"80" then motor_data_int(7)(to_integer(unsigned(motor_id)-113)) <= motor_type & std_logic_vector(unsigned(motor_id) - 112) & motor_pos; end if;
                if motor_id >= x"81" AND motor_id <= x"90" then motor_data_int(8)(to_integer(unsigned(motor_id)-129)) <= motor_type & std_logic_vector(unsigned(motor_id) - 128) & motor_pos; end if;
                if motor_id >= x"91" AND motor_id <= x"A0" then motor_data_int(9)(to_integer(unsigned(motor_id)-145)) <= motor_type & std_logic_vector(unsigned(motor_id) - 144) & motor_pos; end if;
                if motor_id >= x"A1" AND motor_id <= x"B0" then motor_data_int(10)(to_integer(unsigned(motor_id)-161)) <= motor_type & std_logic_vector(unsigned(motor_id) - 160) & motor_pos; end if;
                if motor_id >= x"B1" AND motor_id <= x"C0" then motor_data_int(11)(to_integer(unsigned(motor_id)-177)) <= motor_type & std_logic_vector(unsigned(motor_id) - 176) & motor_pos; end if;
                if motor_id >= x"C1" AND motor_id <= x"D0" then motor_data_int(12)(to_integer(unsigned(motor_id)-193)) <= motor_type & std_logic_vector(unsigned(motor_id) - 192) & motor_pos; end if;
                if motor_id >= x"D1" AND motor_id <= x"E0" then motor_data_int(13)(to_integer(unsigned(motor_id)-209)) <= motor_type & std_logic_vector(unsigned(motor_id) - 208) & motor_pos; end if;
                if motor_id >= x"E1" AND motor_id <= x"F0" then motor_data_int(14)(to_integer(unsigned(motor_id)-225)) <= motor_type & std_logic_vector(unsigned(motor_id) - 224) & motor_pos; end if;   
            elsif data_req_main_int = '0' AND data_ack_m_flag = '1' then
                data_ack_m_flag <= '0'; 
                data_ack_m_out <= '0'; 
                motor_next_state <= motor_write_data;
            end if;
        
        when motor_write_data =>
            data_req_int_flag <= '1';
            data_ack_int := data_ack_int_flag;
            if data_ack_int = '1' then
                data_req_int_flag <= '0';
                motor_data_int_master <= motor_data_int;
                motor_next_state <= motor_read_data;
            else motor_next_state <= motor_read_data;    
            end if;        
    end case;        
    end if;
end if;         
end process;

synch_strobe: process(tosnet_synch)
begin
if rising_edge(tosnet_synch) then
    strobe_signal <= strobe_signal + 1;
end if;
end process;

bit_flip_tos: process(system_response_tos, adc_data_read_tos_internal)
variable system_response_current_tos, system_response_old_tos : std_logic := '0'; 
variable internal_response_tos_current, internal_response_tos_old : std_logic := '0';
begin
if tosnet_enabled = '1' AND master_check = '0' then
    system_response_current_tos := system_response_tos;
    if system_response_current_tos = NOT system_response_old_tos then 
        system_response_old_tos := system_response_current_tos;
        msb_tos <= "01";
    end if;
    
    internal_response_tos_current := adc_data_read_tos_internal;        
    if (internal_response_tos_current = NOT internal_response_tos_old) then 
        msb_tos <= "00"; 
        internal_response_tos_old := internal_response_tos_current;
    end if; 
end if; 
end process;
   
OUTPUT_DECODE: process (memory_clock)

begin
    if rising_edge(memory_clock) then

     if tosnet_enabled = '1' AND master_check = '1' then
            node_sensor_out      <= data_to_memory_master;       --send sensor data to main memory state machine             
            data_tosnet_out      <= data_to_tosnet_master;       --send motor data to TosNet for slave
            network_addr         <= update_addr_master;
            network_clk          <= set_network_clk_master;
            network_we           <= write_enable_master; 
            data_network_rcommit <= rcommit_mas;
            data_network_wcommit <= wcommit_mas;
            data_req_s_out       <= data_req_s_flag;
            system_response_out  <= system_response;         --TESTING
            synch_interrupt      <= master_interrupt;        
     --TosNet enabled, slave - send sensor data to TosNet, send motor data from master to local motors
     elsif (tosnet_enabled = '1' AND master_check = '0') then   
            data_tosnet_out <= data_to_tosnet_slave;            --data from slave sensors written to TosNet
            network_addr <= update_addr_slave;
            network_clk  <= set_network_clk_slave;
            network_we           <= write_enable_slave;
            motor_data_tos_out   <= motor_data_signal;
            --data_ack_s_out       <= data_ack_s_flag;
            data_req_m_out       <= data_req_m_flag;
            data_network_rcommit <= rcommit_sl;
            data_network_wcommit <= wcommit_sl;  
            system_response_out_tos <= system_response_tos;
     end if;
    end if;
end process; 

end Behavioral;