library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity debounce is
    Port ( clock : in STD_LOGIC;
           switch_in : in STD_LOGIC;
           switch_out : out STD_LOGIC);
end debounce;

architecture Behavioral of debounce is

    signal prescaler        : unsigned(31 downto 0) := x"00000000";
    signal internal_clock   : std_logic;

begin

    internal_clock <= prescaler(10); 
    
prescale:
process(clock)

begin
    if rising_edge(clock) then
        prescaler <= prescaler + 1;
    end if;
    
end process;

output_debounce:
process(internal_clock)

begin
    if rising_edge(internal_clock) then
        switch_out <= switch_in;
    end if;
end process;

end Behavioral;