## Constraints file for memory interface

##Clock signal
set_property -dict {PACKAGE_PIN K17 IOSTANDARD LVCMOS33} [get_ports clock_125M]
create_clock -period 8.000 -name sys_clk_pin -waveform {0.000 4.000} -add [get_ports clock_125M]

#set_property -dict {PACKAGE_PIN K17 IOSTANDARD LVCMOS33} [get_ports clock_50M]
#create_clock -period 20.000 -name sys_clk_pin -waveform {0.000 10.000} -add [get_ports clock_50M]   #50MHz has clock period of 20ns, waveform gives rising and falling edge (0 to half period)

##Pmod Header JA (XADC)
set_property -dict {PACKAGE_PIN N15 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p14]
set_property -dict {PACKAGE_PIN L14 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p7]
set_property -dict {PACKAGE_PIN K16 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p15]
set_property -dict {PACKAGE_PIN K14 IOSTANDARD LVCMOS33} [get_ports Vaux_v_p6]
set_property -dict {PACKAGE_PIN N16 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n14]
set_property -dict {PACKAGE_PIN L15 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n7]
set_property -dict {PACKAGE_PIN J16 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n15]
set_property -dict {PACKAGE_PIN J14 IOSTANDARD LVCMOS33} [get_ports Vaux_v_n6]

##Pmod Header JC
#set_property -dict {PACKAGE_PIN V15 IOSTANDARD LVCMOS33} [get_ports sig_in]
#set_property -dict { PACKAGE_PIN W15   IOSTANDARD LVCMOS33     } [get_ports { jc[1] }]; #IO_L10N_T1_34 Sch=jc_n[1]
#set_property -dict { PACKAGE_PIN T11   IOSTANDARD LVCMOS33     } [get_ports { jc[2] }]; #IO_L1P_T0_34 Sch=jc_p[2]
#set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33     } [get_ports { jc[3] }]; #IO_L1N_T0_34 Sch=jc_n[2]
#set_property -dict {PACKAGE_PIN W14 IOSTANDARD LVCMOS33} [get_ports sig_out]
#set_property -dict { PACKAGE_PIN Y14   IOSTANDARD LVCMOS33     } [get_ports { jc[5] }]; #IO_L8N_T1_34 Sch=jc_n[3]
#set_property -dict { PACKAGE_PIN T12   IOSTANDARD LVCMOS33     } [get_ports { jc[6] }]; #IO_L2P_T0_34 Sch=jc_p[4]
#set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33     } [get_ports { jc[7] }]; #IO_L2N_T0_34 Sch=jc_n[4]

##Pmod Header JC (Motors)
set_property -dict {PACKAGE_PIN V15 IOSTANDARD LVCMOS33} [get_ports {motor_out[0]}]
set_property -dict {PACKAGE_PIN W15 IOSTANDARD LVCMOS33} [get_ports {motor_out[1]}]
set_property -dict {PACKAGE_PIN T11 IOSTANDARD LVCMOS33} [get_ports {motor_out[2]}]
set_property -dict {PACKAGE_PIN T10 IOSTANDARD LVCMOS33} [get_ports {motor_out[3]}]
set_property -dict {PACKAGE_PIN W14 IOSTANDARD LVCMOS33} [get_ports {motor_out[4]}]
set_property -dict {PACKAGE_PIN Y14 IOSTANDARD LVCMOS33} [get_ports {motor_out[5]}]
set_property -dict {PACKAGE_PIN T12 IOSTANDARD LVCMOS33} [get_ports {motor_out[6]}]
set_property -dict {PACKAGE_PIN U12 IOSTANDARD LVCMOS33} [get_ports {motor_out[7]}]

##Pmod Header JD (Motors)
set_property -dict {PACKAGE_PIN T14 IOSTANDARD LVCMOS33} [get_ports {motor_out[8]}]
set_property -dict {PACKAGE_PIN T15 IOSTANDARD LVCMOS33} [get_ports {motor_out[9]}]
set_property -dict {PACKAGE_PIN P14 IOSTANDARD LVCMOS33} [get_ports {motor_out[10]}]
set_property -dict {PACKAGE_PIN R14 IOSTANDARD LVCMOS33} [get_ports {motor_out[11]}]
set_property -dict {PACKAGE_PIN U14 IOSTANDARD LVCMOS33} [get_ports {motor_out[12]}]
set_property -dict {PACKAGE_PIN U15 IOSTANDARD LVCMOS33} [get_ports {motor_out[13]}]
set_property -dict {PACKAGE_PIN V17 IOSTANDARD LVCMOS33} [get_ports {motor_out[14]}]
set_property -dict {PACKAGE_PIN V18 IOSTANDARD LVCMOS33} [get_ports {motor_out[15]}]

##OLD CONSTRAINTS (only JD used for testing originally)
#set_property -dict {PACKAGE_PIN T14 IOSTANDARD LVCMOS33} [get_ports sig_in]
#set_property -dict {PACKAGE_PIN T15 IOSTANDARD LVCMOS33} [get_ports test_out]   #IO_L5N_T0_34 Sch=jd_n[1]
#set_property -dict {PACKAGE_PIN P14 IOSTANDARD LVCMOS33} [get_ports motor_out]
#set_property -dict {PACKAGE_PIN U14 IOSTANDARD LVCMOS33} [get_ports sig_out]

##Pmod Header JE (Motors)
#set_property -dict {PACKAGE_PIN V12 IOSTANDARD LVCMOS33} [get_ports {motor_out[8]}]
#set_property -dict {PACKAGE_PIN W16 IOSTANDARD LVCMOS33} [get_ports {motor_out[9]}]
#set_property -dict {PACKAGE_PIN J15 IOSTANDARD LVCMOS33} [get_ports {motor_out[10]}]
#set_property -dict {PACKAGE_PIN H15 IOSTANDARD LVCMOS33} [get_ports {motor_out[11]}]
#set_property -dict {PACKAGE_PIN V13 IOSTANDARD LVCMOS33} [get_ports {motor_out[12]}]
#set_property -dict {PACKAGE_PIN U17 IOSTANDARD LVCMOS33} [get_ports {motor_out[13]}]
#set_property -dict {PACKAGE_PIN T17 IOSTANDARD LVCMOS33} [get_ports {motor_out[14]}]
#set_property -dict {PACKAGE_PIN Y17 IOSTANDARD LVCMOS33} [get_ports {motor_out[15]}]

##Pmod Header JE (TosNet)
set_property -dict {PACKAGE_PIN V12 IOSTANDARD LVCMOS33} [get_ports sig_out]
set_property -dict {PACKAGE_PIN W16 IOSTANDARD LVCMOS33} [get_ports system_test_bit]
set_property -dict {PACKAGE_PIN J15 IOSTANDARD LVCMOS33} [get_ports system_test_bit_tos]
#set_property -dict {PACKAGE_PIN H15 IOSTANDARD LVCMOS33} [get_ports {motor_out[11]}]
set_property -dict {PACKAGE_PIN V13 IOSTANDARD LVCMOS33} [get_ports sig_in]
#set_property -dict {PACKAGE_PIN U17 IOSTANDARD LVCMOS33} [get_ports {motor_out[13]}]
#set_property -dict {PACKAGE_PIN T17 IOSTANDARD LVCMOS33} [get_ports {motor_out[14]}]
#set_property -dict {PACKAGE_PIN Y17 IOSTANDARD LVCMOS33} [get_ports {motor_out[15]}]

##LEDs
set_property -dict {PACKAGE_PIN M14 IOSTANDARD LVCMOS33} [get_ports {led_indicator[0]}]
set_property -dict {PACKAGE_PIN M15 IOSTANDARD LVCMOS33} [get_ports {led_indicator[1]}]
set_property -dict {PACKAGE_PIN G14 IOSTANDARD LVCMOS33} [get_ports {led_indicator[2]}]
set_property -dict {PACKAGE_PIN D18 IOSTANDARD LVCMOS33} [get_ports {led_indicator[3]}]

##Switches
set_property -dict {PACKAGE_PIN G15 IOSTANDARD LVCMOS33} [get_ports {sw_setting[0]}]
set_property -dict {PACKAGE_PIN P15 IOSTANDARD LVCMOS33} [get_ports {sw_setting[1]}]
set_property -dict {PACKAGE_PIN W13 IOSTANDARD LVCMOS33} [get_ports {sw_setting[2]}]
set_property -dict {PACKAGE_PIN T16 IOSTANDARD LVCMOS33} [get_ports {sw_setting[3]}]
