library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity set_tosnet_id is
    Port ( id1 : in STD_LOGIC;
           id2 : in STD_LOGIC;
           id3 : in STD_LOGIC;
           set_node_id : out STD_LOGIC_VECTOR (3 downto 0)
          );
end set_tosnet_id;

architecture Behavioral of set_tosnet_id is

begin

set_node_id <= "0" & id3 & id2 & id1;

end Behavioral;
