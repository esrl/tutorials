library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pc_pwm_v10 is
 Port (   clk_50MHz         : in STD_LOGIC;
         tosnet_enabled   : in STD_LOGIC;
         tosnet_master    : in STD_LOGIC;
         data_req_loc_pwm     : in STD_LOGIC;
         data_req_tos_pwm     : in STD_LOGIC;
         data_ack_loc_pwm     : out STD_LOGIC;
         data_ack_tos_pwm     : out STD_LOGIC := '0';
         motor_data_local : in STD_LOGIC_VECTOR (31 downto 0);
         motor_data_tosnet : in STD_LOGIC_VECTOR (31 downto 0);
         pulse_out        : out STD_LOGIC_VECTOR (15 downto 0) := x"0000"
         ); 
end pc_pwm_v10;

architecture Behavioral of pc_pwm_v10 is

--motor signals
type pwm_motor_array is array (0 to 15) of std_logic_vector(15 downto 0);
signal motor_data_int : STD_LOGIC_VECTOR (31 downto 0); 
signal motor_data_tos : STD_LOGIC_VECTOR (31 downto 0);

signal motor_data_array, motor_data_tos_array : pwm_motor_array;

signal data_ack_loc_int, data_ack_tos_int : STD_LOGIC;

--signals for prescaling main clock
signal prescale_clock : unsigned(31 downto 0) := x"00000000";
signal pwm_clock, pwm_clock_tos   : std_logic;

begin
 
     pwm_clock <= (prescale_clock(1));
     pwm_clock_tos <= (prescale_clock(1));  
     
prescale: process(clk_50MHz)
begin
   if rising_edge(clk_50MHz) then
       prescale_clock <= prescale_clock + 1;
   end if;    
end process; 

read_motor_positions: process(pwm_clock)     
variable motor_msb_int, motor_msb_tos    : STD_LOGIC_VECTOR (0 downto 0) := "0";
variable data_req_flag_int, data_req_flag_tos_int : STD_LOGIC; 

variable motor_id_int, motor_id_tos : STD_LOGIC_VECTOR (7 downto 0) := x"01";
variable motor_pos_int, motor_pos_tos : STD_LOGIC_VECTOR (15 downto 0) := x"0000";

begin
if rising_edge(pwm_clock) then
   if tosnet_enabled = '0' then
       data_req_flag_int := data_req_loc_pwm;
       if data_req_flag_int = '1' AND data_ack_loc_int = '0' then
           data_ack_loc_int <= '1'; 
           data_ack_loc_pwm <= '1'; 
           motor_id_int := motor_data_local(23 downto 16);
           motor_pos_int := motor_data_local(15 downto 0);
           motor_data_array(to_integer(unsigned(motor_id_int)-1)) <= motor_pos_int;
       elsif data_req_flag_int = '0' AND data_ack_loc_int = '1' then
           data_ack_loc_int <= '0';
           data_ack_loc_pwm <= '0';
       end if;                                       
   end if;                     
end if;    
end process;

read_motor_positions_tos: process(pwm_clock_tos)     
variable motor_msb_int, motor_msb_tos    : STD_LOGIC_VECTOR (0 downto 0) := "0";
variable data_req_flag_int, data_req_flag_tos_int : STD_LOGIC; 

variable motor_id_int, motor_id_tos : STD_LOGIC_VECTOR (7 downto 0) := x"01";
variable motor_pos_int, motor_pos_tos : STD_LOGIC_VECTOR (15 downto 0) := x"0000";

begin
if rising_edge(pwm_clock_tos) then
    if tosnet_enabled = '1' AND tosnet_master = '0' then
       data_req_flag_tos_int := data_req_tos_pwm;
       if data_req_flag_tos_int = '1' AND data_ack_tos_int = '0' then
           data_ack_tos_int <= '1';  
           data_ack_tos_pwm <= '1';
           motor_id_tos := motor_data_tosnet(23 downto 16);
           motor_pos_tos := motor_data_tosnet(15 downto 0);
           motor_data_tos_array(to_integer(unsigned(motor_id_tos)-1)) <= motor_pos_tos;
       elsif data_req_flag_tos_int = '0' AND data_ack_tos_int = '1' then
           data_ack_tos_int <= '0';
           data_ack_tos_pwm <= '0';
       end if;                                        
   end if;                     
end if;    
end process;


pwm_all_proc: process(clk_50MHz)
variable motor_data_array_storage, motor_data_tos_array_storage : pwm_motor_array;
variable counter_current, counter_old, counter_current_tos, counter_old_tos : unsigned(31 downto 0) := x"00000000";

variable pwm_counter, pwm_counter_tos : unsigned (19 downto 0) := x"00000";
variable ocr_counter, ocr_counter_tos : unsigned (19 downto 0) := x"00000";
variable cycle_counter, cycle_counter_tos :   unsigned(31 downto 0) := x"00000000";

begin
if rising_edge(clk_50MHz) then
    
    if tosnet_enabled = '0' then
        if pwm_counter < 1000000 then  
            pwm_counter := pwm_counter + 1;
            if pwm_counter < 500000 then ocr_counter := ocr_counter + 1; 
            elsif pwm_counter >= 500000 then ocr_counter := ocr_counter - 1; 
            else ocr_counter := x"00000";
            end if;
        elsif pwm_counter >= 1000000 then 
            pwm_counter := x"00000"; 
            ocr_counter := x"00000"; 
            cycle_counter := cycle_counter + 1;
        else pwm_counter := x"00000"; ocr_counter := x"00000";
        end if;
        counter_current := cycle_counter; 
        if counter_current /= counter_old then
            counter_old := counter_current;
            motor_data_array_storage := motor_data_array;
        end if;        
        for i in 0 to 15 loop
        --signal high when counter <= OCR else signal low
        --for 16-bit, OCR = 37500 is equivalent to .75ms (minimum for old servos); 27200 is equivalent to .544ms (minimum for new servos)
            if ocr_counter <= (x"03520" + (unsigned(motor_data_array_storage(i)) srl 1)) then          --minimum is 13600 since only counting on rising edge 
               pulse_out(i) <= '1';
            elsif ocr_counter > (x"03520" + (unsigned(motor_data_array_storage(i)) srl 1)) then
               pulse_out(i) <= '0';
            else pulse_out(i) <= '0';
            end if;    
        end loop;
      
    elsif tosnet_enabled = '1' AND tosnet_master = '0' then 
        
        if pwm_counter_tos < 1000000 then  
            pwm_counter_tos := pwm_counter_tos + 1;
            if pwm_counter_tos < 500000 then ocr_counter_tos := ocr_counter_tos + 1; 
            elsif pwm_counter_tos >= 500000 then ocr_counter_tos := ocr_counter_tos - 1; 
            else ocr_counter_tos := x"00000";
            end if;
        elsif pwm_counter_tos >= 1000000 then 
            pwm_counter_tos := x"00000"; 
            ocr_counter_tos := x"00000"; 
            cycle_counter_tos := cycle_counter_tos + 1;
        else pwm_counter_tos := x"00000"; ocr_counter_tos := x"00000";
        end if;
        
        counter_current_tos := cycle_counter_tos;
        if counter_current_tos /= counter_old_tos then
            counter_old_tos := counter_current_tos;
            motor_data_tos_array_storage := motor_data_tos_array;
        end if; 
        for j in 0 to 15 loop
            if ocr_counter_tos <= (x"03520" + (unsigned(motor_data_tos_array_storage(j)) srl 1)) then          --minimum is 13600 since only counting on rising edge 
               pulse_out(j) <= '1';
            elsif ocr_counter_tos > (x"03520" + (unsigned(motor_data_tos_array_storage(j)) srl 1)) then
               pulse_out(j) <= '0';
            else pulse_out(j) <= '0';
            end if;    
        end loop;      
        
    end if;
    
end if;    
end process;

--output_process: process(clk_50MHz)
--begin
--    if rising_edge(clk_50MHz) then
--        if tosnet_enabled = '0' then
--            data_ack_loc_pwm <= data_ack_loc_int;
--        elsif tosnet_enabled = '1' AND tosnet_master = '0' then 
--            data_ack_tos_pwm <= data_ack_tos_int;    
--        end if;
--    end if;
--end process;

end Behavioral;